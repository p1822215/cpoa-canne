<?php
    //On récupère la date et on lui change son format
    $dateString = date('d/m/Y', strtotime($dateF.'+1 week'));
    $dat = date('m/d/Y', strtotime($dateF.'+1 week'));
?>


<div class="calendrier col-12 mt-5">
    <div class="row justify-content-center">
        <h3><span class=""><a href="index.php?page=listeReservations&semaine=1"> < </a>Semaine du <?php echo $dateString?></span></h3>
    </div>
        
    <table id="calendrier" class="table table-bordered mt-4"> 
        <tr>
            <!--Affichage des dates -->
            <?php
                //On affiche la date +i jours
                for($i=0;$i<7;$i++)
                {
                    $datStr = strtotime($dat."+".$i."days");
                    $datString = date('d/m/Y', $datStr);
                ?>
                    <th><?php echo $datString ?></th>
                <?php
                }
            ?>
            
            
            
        

        </tr>
                                
    </table>
</div>