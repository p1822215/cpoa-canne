<?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
<?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>

<?php 
    $stylesArray=array("listeReservations");
    require_once(PATH_VIEWS."header.php");
    
    $donnees = array("listeInvites" => true);
	$donneeRecu = appelAPI('GET', API_LINK.'reservations.php', $donnees);
?>
<link rel="stylesheet" href=<?php echo PATH_CSS."listeReservations.css"?>>
<script src="functions/fonctions.js"></script>

</head>
    <body style="background-color: #F3F3F3;">
        <div class="container">

            <?php 
                // Affiche le haut de la page avec le nom et prénom de l'utilisateur
                
                $boutonRetour = false;
                require_once(PATH_VIEWS."hautPage.php");
            ?>

        <!--Bouton paramètres-->
        <div class="row mt-5 pl-lg-5 pl-0">
            <button class="ui grey animated button m-lg-0 mx-auto" tabindex="0" onclick="window.location.href = 'index.php?page=parametres';">
                <div class="visible content">Paramètres</div>
                <div class="hidden content">
                    <i class="fas fa-wrench"></i>
                </div>
            </button>
        </div>
           
        <!--Bouton ajouter une réservation-->
        <div class="row my-3">
                <button class="ui right labeled icon button yellow mx-auto" onclick="window.location.href = 'index.php?page=ajoutReservations';" >
                <i class="plus circle icon"></i>
                Ajouter une réservation
                </button>
        </div>

            <!-- Titre -->
            <div class="row mt-5">
                <p class="text-center col mt-4" style="font-weight: bold; font-size: 1.6em;">Liste des réservations</p>
            </div>
            <div id="chargementId"></div>
            <?php
                if($dateF!=null)
                {
                ?>
                    <!--Affichage de la liste des invités-->
                    <div class="contenir-liste mt-5"> 
                        <div class="listeInvites">
                            <h3 class="text-center">Affichage des invités</h3>
                            
        
                            <select type="hidden" name="invites[]" multiple="10" class="ui fluid search multiple selection dropdown row col-lg-9 col-12 mx-auto">
                                <option value="">Selectionnez jusqu'à 6 invités</option>
                                
                                <?php
                                    foreach($donneeRecu as $unInviteDonnee)
                                    {
                                    ?>
                                        <option value=<?= '"'.$unInviteDonnee['idVIP'].'"';?>><?=$unInviteDonnee['nomVIP']." ".$unInviteDonnee['prenomVIP']?></option>
                                    <?php
                                    }
                                    
                                ?>
                            </select>
        
                        </div>
                    </div>
                    
                    <!--Afficher en fonction de la semaine 1 ou 2-->
                    <?php
                        if (isset($_GET['semaine']))
                        {
                            $semaine = htmlspecialchars($_GET['semaine']);
                            if($semaine==2)
                            {
                                require_once(PATH_VIEWS."semaine2.php");
                            }
                            else
                            {
                                require_once(PATH_VIEWS."semaine1.php");
                            }

                        }
                        else
                        {
                            require_once(PATH_VIEWS."semaine1.php");
                        }
                    ?>
                   
    

                <?php
                }
                else
                {
                ?>
                    <h1 class="text-center">Pas de prochaine date prévue. Allez sur les paramètres</h1>
                <?php   
                }
            ?>



        </div>

        <script>
            //Chargement lorsque la page se charge
            const chargement = document.createElement('div');
            chargement.className = `row col-8 mx-auto flex-column my-4`;
            chargement.id = `chargement`;
            chargement.style = `background-color: white; border-radius: 10px; z-index: 0;`;
            chargement.innerHTML = `
                <img src="assets/images/loading.gif" class="d-block mx-auto" style="width: 100px;">
                <p class="col-7 text-center mx-auto mb-4" style="font-size: 0.9em; color: gray">Chargement ...</p>
            `;

            //Affichage de la combo-box et action lorsqu'un invité est choisi ou retiré       
            $('.ui.dropdown').dropdown({
              maxSelections: 6,
              onChange: 
              function(value, text, $selectedItem) {
                
                afficherReservation(value);
    		}
          });

          //Fonction déconnexion
          function deconnexion() {
            $('.tiny.modal.deconnexion')
            .modal({
                blurring: true,
                onApprove : function() {
                    window.location.href = 'index.php?deconnexion=true';
                }
            })
            .modal('show')
        }

        //Fonction qui supprime des réservations sur le calendrier à chaque fois que la fonction est appelée
          function removeElementsByClass(c){
                var elements = document.getElementsByClassName(c);
                while(elements.length > 0){
                    elements[0].parentNode.removeChild(elements[0]);
                }
          };
          
          //Fonction qui permet d'afficher une réservation
          function afficherReservation(tableau)
          {
              document.getElementById("chargementId").appendChild(chargement);
              removeElementsByClass("ligne");
              removeElementsByClass("undefined");
              tabColor = ["red","orange","cyan","green","blue","purple","gold","brown","pink","gray"];
              var i = 0;
              var ar = [];
              if(tableau.length==0)
              {
                if (chargement != null) {
                        chargement.remove();
                    }
              }
              for(const unInvite of tableau)
              {
                ar=[];
                donnees = {"idVIPReser" : unInvite};
                appelAPI("GET", apiURL + "reservations.php", donnees)
                    .then(data => {
                        ar=[];
                        var some = jQuery.parseJSON(data);

                        if(some.length!=0) //On affiche une réservation
                        {
                            ar.push(some[0]["nomVIP"]);
                            ar.push(some[0]["prenomVIP"]);
                            console.log(ar[0]);  
                            console.log(ar[1]);  
                            
                            let element = document.createElement("tr");

                            element.style = `background-color: ${tabColor[i]};`;
                            element.className = "ligne";
                            element.innerHTML = `<td><a style="color:white;" href="index.php?page=detailReservation&id=`+some[0]["idReservation"]+`">Réservation de ${ar[0]} ${ar[1]}</a></td>`;
                            for(j=0;j<6;j++)
                            {
                                element.innerHTML+= `<td><a style="color:white;" href="index.php?page=detailReservation&id=`+some[0]["idReservation"]+`">Réservation de ${ar[0]} ${ar[1]}</a></td>`;
                            }
                            document.getElementById('calendrier').appendChild(element);
                            i++;
                        }
                        else //On affiche que le VIP n'a pas de réservation
                        {
                            donnees = {"idVIP" : unInvite};
                            appelAPI("GET", apiURL + "reservations.php", donnees)
                                .then(data => {
                                ar=[];
                                var some2 = jQuery.parseJSON(data);
                                console.log(unInvite);
                                ar.push(some2[0]["nomVIP"]);
                                ar.push(some2[0]["prenomVIP"]);
                                let element = document.createElement("tr");
                                element.className = "undefined";
                                element.innerHTML = `<td>Pas de réservation pour ${ar[0]} ${ar[1]}</td>`;
                                for(j=0;j<6;j++)
                                {
                                    element.innerHTML+= `<td>Pas de réservation pour ${ar[0]} ${ar[1]}</td>`;
                                }
                                document.getElementById('calendrier').appendChild(element);
                            
                        });   
                    }
                    if (chargement != null) {
                        chargement.remove();
                    }
                    
                    });
              }
              
           };

            



           
        </script>
        <!-- Modal pour la déconnexion -->
        <div class="ui tiny modal deconnexion" style="position: relative; height: 200px;;">
            <div class="header">
                Déconnexion
            </div>
            <div class="content">
                <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
            </div>
            <div class="actions">
                <div class="ui cancel button">
                    Annuler
                </div>
                <div class="ui ok red button">
                    Me déconnecter
                </div>
            </div>
        </div>

    </body>

</html>




