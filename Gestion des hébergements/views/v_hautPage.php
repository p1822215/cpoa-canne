

<!-- Header -->
<header class="row justify-content-start mt-3 align-items-center">
    <div class="col-lg-2 col-4 m-0 p-0 mx-auto mt-lg-0 mt-3">
        <?php echo '<img src="'.PATH_LOGO.'" alt="" class="d-block col-11 ml-lg-auto ml-0">';?>
    </div>
    <div class="row align-items-center m-0 p-0 ml-lg-3 ml-0 my-lg-0 my-3 col-lg col-12 text-lg-left text-center">
        <p class="col-12 m-0 p-0" style="font-size: 1.9em; color: black; font-weight: bold;">Interface de gestion des <?php if($espace=="Gerant"){echo "hébergements";} else {echo "réservations";}?></p>
        <p class="col-12 m-0 p-0" style="font-size: 1.3em; color: gray;">Espace <?php echo $espace?> - <?php echo $nom?> <?php echo $prenom?></p>
    </div>
    <div class="d-flex ml-lg-auto mx-auto mt-lg-0 mt-3 mr-4 justify-content-center align-items-center m-0 p-0">
        <button class="ui vertical red  animated button" tabindex="0" onclick="deconnexion()">
            <div class="visible content">Déconnexion</div>
            <div class="hidden content">
                <i class="sign-out icon"></i>
            </div>
        </button>
    </div>
</header>

<?php  
    // Affiche ou non le bouton retour et en fonction de la fonction renvoi à des pages différentes
    if ($boutonRetour == true && $_COOKIE['fonction']=="Gerant") {
        echo '
        <!-- Bouton retour -->
        <div class="row mt-5 pl-lg-5 pl-0">
            <button class="ui yellow animated button m-lg-0 mx-auto" tabindex="0" onclick="window.location.href = \'index.php?page=listeHebergements\';">
                <div class="visible content">Retour</div>
                <div class="hidden content">
                    <i class="arrow left icon"></i>
                </div>
            </button>
        </div>
        ';
    } elseif ($boutonRetour == true && $_COOKIE['fonction']=="responsable") {
        echo '
        <!-- Bouton retour -->
        <div class="row mt-5 pl-lg-5 pl-0">
            <button class="ui yellow animated button m-lg-0 mx-auto" tabindex="0" onclick="window.location.href = \'index.php?page=listeReservations\';">
                <div class="visible content">Retour</div>
                <div class="hidden content">
                    <i class="arrow left icon"></i>
                </div>
            </button>
        </div>
        ';
    }
?>