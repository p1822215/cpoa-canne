<?php
	$titre="Se connecter";
	$stylesArray=array("login");
	require_once(PATH_VIEWS."header.php");
?>
		</head>

	<body class="bg-light container">
		 
		<div class="container row div-form">
			<div class="col-lg-2 col-4 m-0 p-0 mx-auto mt-lg-0 mt-3">
                <?php echo '<img src="'.PATH_LOGO.'" alt="" class="d-block col-11 ml-lg-auto ml-0">';?>
            </div>
			<h4 class="col-12 text-center font-weight-bold boldh4 mt-3">Interface de gestion des hébergements</h4>

			<?php
				if(isset($alert)) //On affiche en cas de mauvais mdp ou de mauvais login
				{
				?>
					<div class="alert alert-danger mx-auto mt-4" style="width:200px;" role="alert">
					  <span class="text-center ml-2"><?= $alert ?></span>
					</div>
				<?php
				}


			?>
			<p class="col-12 text-center mt-5 boldp"> Veuillez vous identifier </p>

			<form method="post" action="index.php" class="text-center mx-auto form-group">
				<input type="input" name="login" size="30" class="form-control border border-white py-1 mx-auto px-3 rounded shadow mt-2" 
				placeholder="Identifiant" required/>
				<br/>
				<input type="password" name="mdp" size="30" class="form-control border border-white py-1 px-3 mx-auto rounded shadow" 
				placeholder="Mot de passe" required/>
				<br/>
				<button type="submit" class="btn btn-outline-danger rounded py-1 px-2 mx-auto mt-4">
				Se connecter</button>
			</form>
		</div>
	</body>

</html>