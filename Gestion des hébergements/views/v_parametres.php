<?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
<?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>
<?php 
    require_once(PATH_VIEWS."header.php");
?>

<?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
<?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>

</head>
    <body style="background-color: #F3F3F3;">
        <div class="container">
            <?php 
                // Affiche le haut de la page avec le nom et prénom de l'utilisateur 
                require_once(PATH_VIEWS."hautPage.php");
			?>
			
			<?php
				if($dateF==null) 
				{
				?>
					<form method="post" action="index.php?page=parametres" class="ui form">
						<h4 class="ui dividing header" style="margin-top:10%;">Ajouter une nouvelle date pour le prochain festival</h4>

						<input type="date" class="col-3" name="datechoix">
						
						
						<button type="submit" class="btn btn-outline-success btn-lg ml-3">Valider</button>
					
					</form>
					<p class="col-5 mt-4 mr-2">Le festival durera 15 jours à partir de la date choisie</p>
				<?php	
				}
				else //La date s'affiche et on peut la modifier
				{
				?>
					<form method="post" action="index.php?page=parametres" class="ui form">
						<h4 class="ui dividing header" style="margin-top:10%;">Modifier la date du prochain festival</h4>

						<input type="date" class="col-3" name="datechoix" value=<?php echo $dateF ?>>
					
					
						<button type="submit" class="btn btn-outline-success btn-lg ml-3">Valider</button>
					
					</form>
				<?php
				}
			?>

			<p>Important : Si vous choisissez une date antérieure à celle d'aujourd'hui, celle-ci sera automatiquement supprimée</p>
			
				
		</div>
	</body>
</head>