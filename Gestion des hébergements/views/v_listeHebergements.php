
    
    <?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
    <?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>

</head>

<body style="background-color: #F3F3F3;">
    <div class="container">

        <?php 
            
            // Affiche le haut de la page avec le nom et prénom de l'utilisateur 
            require_once(PATH_VIEWS."hautPage.php");
        ?>

        <!-- Titre -->
        <div class="row mt-5">
            <p class="text-center col mt-5" style="font-weight: bold; font-size: 1.6em;">Hébergements disponibles</p>
        </div>

        <!-- Bouton ajouter -->
        <div class="row my-3">
            <button class="ui right labeled icon button yellow mx-auto" onclick="window.location.href = 'index.php?page=ajoutHebergements';" >
            <i class="plus circle icon"></i>
            Ajouter un hébergement
            </button>
        </div>

        <!-- Liste des hébergements -->
        <div class="row">
            <div class="col-lg-5 col-md-7 col-10 mx-auto m-0 p-0">

                <?php 
                    $donneesUt = array('idUtilisateur' => $id);
                    $donneeRecu = appelAPI('GET', API_LINK.'hebergements.php', $donneesUt);
                    if (sizeof($donneeRecu) == 0) {
                        echo '<div class="row col ui compact message mx-auto text-center my-5">
                        <p class="text-center">Aucun hébergement.</p>
                        </div>';
                    } else {
                        foreach($donneeRecu as $unHebergement) {
                            echo '
                            <div class="row mx-auto col m-0 px-3 py-2 my-3" style="background-color: white; border-radius: 12px; cursor: pointer;" onclick="document.getElementById(\'formHebergement'.$unHebergement['idHebergement'].'\').submit()">
                                <div class="d-flex m-0 p-0 flex-column col-8">
                                    <p class="m-0 p-0 my-1" style="font-weight: bold; font-size: 1.5em;">'.$unHebergement['nomHebergement'].'</p>
                                    <p class="m-0 p-0 my-1" style="font-size: 1.3em; color: gray;">Type: '.$unHebergement['libelleType'].'</p>
                                    <p class="m-0 p-0 my-1" style="font-size: 1.3em; color: gray;">'.$unHebergement['nbService'].' services disponibles</p>
                                </div>
                                <div class="d-flex m-0 p-0 flex-column justify-content-between text-right col-4 my-1">
                                    <i class="grey large arrow alternate circle right icon ml-auto m-1"></i>
                                    <p class="m-0 p-0 my-211" style="font-weight: bold; font-size: 1.3em; color: orange;"></p>
                                </div>
                            </div>
                            <form method="post" id="formHebergement'.$unHebergement['idHebergement'].'" action="index.php?page=detailHebergement">
                                <input type="hidden" name="idHebergement" value="'.$unHebergement['idHebergement'].'">
                            </form>
                            ';
                        }
                    }
                  
                ?>
            </div>
        </div>
        
        <!-- Pied de page -->
        <footer class="row mt-3">
            <p class="text-center col mb-3" style="font-size: 1em;">Baptiste Faure, Camélia Méraoui - Projet Cannes IUT Lyon 1</p>
        </footer>
    </div>
</body>

<script>

/* Affiche la fenêtre modale lors de l'appel de cette fonction */
function deconnexion() {
    $('.tiny.modal')
    .modal({
        blurring: true,
        onApprove : function() {
            window.location.href = 'index.php?deconnexion=true';
        }
    })
    .modal('show')
}
</script>

<!-- Modal pour la déconnexion -->
<div class="ui tiny modal" style="position: relative; height: 200px;;">
    <div class="header">
        Déconnexion
    </div>
    <div class="content">
        <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
    </div>
    <div class="actions">
        <div class="ui cancel button">
          Annuler
        </div>
        <div class="ui ok red button">
            Me déconnecter
        </div>
    </div>
</div>


</html>