<?php
	require_once(PATH_VIEWS."header.php");
?>

<?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
<?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>

<script src="functions/fonctions.js"></script>

</head>

<body style="background-color: #F3F3F3;">
    <div class="container" id="main">
        <!-- Header -->
        <?php 
            $boutonRetour = true;
            require_once(PATH_VIEWS."hautPage.php");
        ?>

        <!-- Combo box avec la liste des invités -->
        <div class="row flex-column justify-content-center align-items-center col-8 mx-auto pb-4 pt-3 mt-5" style="background-color: white; border-radius: 10px">
            <p class="font-weight-bold" style="font-size: 1.55em">Séléctionnez un invité</p>
            <?php
                $donneesUt = array('listeInvites' => 'true');
                $donneeRecu = appelAPI('GET', API_LINK.'reservations.php', $donneesUt);
                echo '
                    <select class="ui search dropdown selection col-7" id="invite" style="z-index: 1;">
                    <option value="">Séléctionnez ou recherchez un invité</option>
                ';
                foreach($donneeRecu as $unInvite) {
                    echo '
                    <option value="'.$unInvite['idVIP'].'" type="'.$unInvite['typeVIP'].'" idGroupeJury="'.$unInvite['groupeJury'].'" idEquipe="'.$unInvite['equipe'].'">'.$unInvite['prenomVIP'].' '.$unInvite['nomVIP'].'</option>
                    ';
                }
                echo '
                    </select>
                ';
            ?>
            <p class="mt-3 col-7 text-center" style="font-size: 0.9em; color: gray">Vous pouvez rechercher un invité en tapant son nom directement dans la liste déroulante</p>
        </div>

        <!-- liste des hébergements disponibles en fonction de l'invité séléctionné -->
        <div class="row">
            <div class="col-lg-6 col-md-7 col-10 mx-auto m-0 p-0 mb-5" id="listeHeber">
                <!-- Rempli en javascript -->

            </div>
        </div>




    </div>

    <script>

        var nom = null;
        var id = null;
        var type = null;
        var idGroupeJury = null;
        var idEquipe = null;

        let listeHeber = document.getElementById("listeHeber");
        let main = document.getElementById("main");

        // Élement d'affichage du chargement
        const chargement = document.createElement('div');
        chargement.className = `row col-8 mx-auto flex-column my-4`;
        chargement.id = `chargement`;
        chargement.style = `background-color: white; border-radius: 10px; z-index: 0;`;
        chargement.innerHTML = `
            <img src="assets/images/loading.gif" class="d-block mx-auto" style="width: 100px;">
            <p class="col-7 text-center mx-auto mb-4" style="font-size: 0.9em; color: gray">Chargement des hébergements disponibles ...</p>
        `;
            
        // Récupération des données lors du changement d'invité
        $('.ui.dropdown').dropdown({
            onChange: function(value, text, $selectedItem) {
                var e = document.getElementById("invite");
                nom = text;
                id = value;
                type = e.options[e.selectedIndex].getAttribute("type");
                idGroupeJury = e.options[e.selectedIndex].getAttribute("idGroupeJury");
                idEquipe = e.options[e.selectedIndex].getAttribute("idEquipe");
                console.log(nom + " " + id + " " + type);
                verifSiDejaReservation();
            }
        });

        // recherche et affiche uniquement les hébergements disponibles par rapport à l'invité
        function search() {            
            if (type === "invite") {
                // Affiche tous les hébergements avec de la place
                var donnees = {"tousAvecPlace" : "true"};
                appelAPI("GET", apiURL + "hebergements.php", donnees)
                    .then(data => {
                        var some = jQuery.parseJSON(data);

                        // Ajoute le titre
                        afficherTitre("Hébergements disponibles");

                        if (some.length == 0) {
                            afficherMessage("Aucun hébergements n'est disponible.");
                        } else {
                            // Afficher les hébergements disponibles
                            for (let i = 0; i < some.length; i++) {
                                console.log(some[i].idHebergement);
                                afficheHebergement(some[i].nomHebergement,some[i].libelleType,some[i].nbService,some[i].idHebergement);
                            }
                        }

                        // Ajoute le divider
                        afficheDivider();

                        if (chargement != null) {
                            chargement.remove();
                        }
                    })
            }
            if (type == 'jury') {
                // 1. Affiche les hébergements des autres membres du même jury (si il y en a et qu'il rest de la place)
                var donnees = {"idGroupe" : idGroupeJury,"type" : "juryID"};
                appelAPI("GET", apiURL + "hebergements.php", donnees)
                    .then(data => {
                        var some = jQuery.parseJSON(data);

                        // Ajoute le titre
                        afficherTitre("Hébergements des autres membres du jury de l'invité");

                        // Afficher les hébergements disponibles
                        if (some.length == 0) {
                            afficherMessage("Aucun membre de ce jury n'a réservé ou alors l'hébergement n'a plus assez de place.");
                        } else {
                            for (let i = 0; i < some.length; i++) {
                                console.log(some[i].idHebergement);
                                afficheHebergement(some[i].nomHebergement,some[i].libelleType,some[i].nbService,some[i].idHebergement);
                            }
                        }

                        // Ajoute le divider
                        afficheDivider();

                        if (chargement != null) {
                            chargement.remove();
                        }
                    })
                // 2. Affiche les autres hébergements avec aucun jury ni aucune équipe
                var donnees = {"jury" : "true"};
                appelAPI("GET", apiURL + "hebergements.php", donnees)
                    .then(data => {
                        var some = jQuery.parseJSON(data);

                        // Ajoute le titre
                        afficherTitre("Autres hébergements disponibles pour cet invité appartenant à un jury");

                        // Afficher les hébergements disponibles
                        if (some.length == 0) {
                            afficherMessage("Aucun hébergement n'est disponible.");
                        } else {
                            for (let i = 0; i < some.length; i++) {
                                console.log(some[i].idHebergement);
                                afficheHebergement(some[i].nomHebergement,some[i].libelleType,some[i].nbService,some[i].idHebergement);
                            }
                        }

                        // Ajoute le divider
                        afficheDivider();

                        if (chargement != null) {
                            chargement.remove();
                        }
                    })

            }
            if (type == 'membreEquipe') {
                // 1. Affiche les hébergements des autres membres de la même équipe (si il y en a)
                var donnees = {"idGroupe" : idEquipe,"type" : "equipeID"};
                appelAPI("GET", apiURL + "hebergements.php", donnees)
                    .then(data => {
                        var some = jQuery.parseJSON(data);

                        // Ajoute le titre
                        afficherTitre("Hébergements des autres membres de l'équipe de l'invité");

                        // Afficher les hébergements disponibles
                        if (some.length == 0) {
                            afficherMessage("Aucun membre de cette équipe n'a réservé ou alors l'hébergement n'a plus d'assez de place.");
                        } else {
                            for (let i = 0; i < some.length; i++) {
                                afficheHebergement(some[i].nomHebergement,some[i].libelleType,some[i].nbService,some[i].idHebergement);
                            }
                        }

                        // Ajoute le divider
                        afficheDivider();

                        if (chargement != null) {
                            chargement.remove();
                        }
                    })
                // 2. Affiche les autres hébergements avec aucun jury
                var donnees = {"equipe" : "true"};
                appelAPI("GET", apiURL + "hebergements.php", donnees)
                    .then(data => {
                        var some = jQuery.parseJSON(data);

                        // Ajoute le titre
                        afficherTitre("Autres hébergements disponibles pour cet invité appartenant à une équipe");

                        // Afficher les hébergements disponibles
                        if (some.length == 0) {
                            afficherMessage("Aucun hébergements n'est disponible.");
                        } else {
                            for (let i = 0; i < some.length; i++) {
                                afficheHebergement(some[i].nomHebergement,some[i].libelleType,some[i].nbService,some[i].idHebergement);
                            }
                        }

                        // Ajoute le divider
                        afficheDivider();

                        if (chargement != null) {
                            chargement.remove();
                        }
                    })
            }
        }

        // retire tous les élément d'une classe
        function removeElementsByClass(className){
            var elements = document.getElementsByClassName(className);
            while(elements.length > 0){
                elements[0].parentNode.removeChild(elements[0]);
            }
        }

        // affiche un hébergement
        function afficheHebergement(nomH, typeH, nbS, idH) {
            const div = document.createElement('div');
            div.className = `row mx-auto col m-0 px-3 py-2 my-3 resultat`;
            div.style = 'background-color: white; border-radius: 12px; cursor: pointer;';

            div.innerHTML = `
                <div class="d-flex m-0 p-0 flex-column col-8">
                    <p class="m-0 p-0 my-1" style="font-weight: bold; font-size: 1.5em;">${String(nomH)}</p>
                    <p class="m-0 p-0 my-1" style="font-size: 1.3em; color: gray;">Type: ${String(typeH)}</p>
                    <p class="m-0 p-0 my-1" style="font-size: 1.3em; color: gray;">${String(nbS)} services disponibles</p>
                </div>
                <div class="d-flex m-0 p-0 flex-column justify-content-between text-right col-4 my-1">
                    <!--<i class="grey large arrow alternate circle right icon ml-auto m-1"></i>-->
                    <button class="ui fluid button" onclick="document.getElementById(\'formHebergement${idH}\').submit()"">Voir le détail</button>
                    <button class="ui fluid yellow button" onclick="reserver(${idH}, '${nomH}')">Réserver</button>
                </div>
                <form method="post" id="formHebergement${idH}" action="index.php?page=detailHebergement">
                    <input type="hidden" name="idHebergement" value="${idH}">
                </form>
            `;
            listeHeber.appendChild(div);
        }
        
        // affiche une barre de séparation
        function afficheDivider() {
            // Ajoute le divider
            const div = document.createElement('div');
            div.className = `ui divider my-4`;
            listeHeber.appendChild(div);
        }
        // affiche un titre
        function afficherTitre(message) {
            const p = document.createElement('p');
            p.className = `text-center col-12 mb-0 mt-4`;
            p.style = 'font-size: 1.8em; font-weight: 1000; font-family: Arial';
            p.innerHTML = `${message}`;
            listeHeber.appendChild(p);
        }

        // affiche un message d'information
        function afficherMessage(message) {
            const div = document.createElement('div');
            div.className = `row col ui compact message mx-auto text-center my-5`;
            div.innerHTML = `
                <p class="text-center" style="font-size: 1.2em">${String(message)}</p>
            `;
            listeHeber.appendChild(div);
        }

        // affiche la fenetre modale de confirmation
        function reserver(idH, nomH) {
            $('.tiny.modal.reserver')
            .modal({
                blurring: true,
                onApprove : function() {
                    var e = document.getElementById("confirm");
                    e.innerHTML = `Chargement ...`;
                    creerReservation(idH);
                    return false;
                }
            })
            .modal('show')
            var l = document.getElementById("confirmationLabel");
            l.innerHTML = `Confirmez vous la création d'une réservation pour <span style="font-weight: 1000; color: orange">${nom}</span> dans l'hébergement <span style="font-weight: 1000; color: orange">${nomH}</span> ? `;
        }

        /* Affiche la fenêtre modale lors de l'appel de cette fonction */
        function deconnexion() {
            $('.tiny.modal.deconnexion')
            .modal({
                blurring: true,
                onApprove : function() {
                    window.location.href = 'index.php?deconnexion=true';
                }
            })
            .modal('show')
        }

        // créer une nouvelle réservation
        function creerReservation(idHe) {
            console.log("called");
            var donnees = {
                "idHebergement" : idHe,
                "idVIP" : id
            };
            if (type == 'jury') {
                donnees.juryID = idGroupeJury;
            }
            if (type == 'membreEquipe') {
                donnees.equipeID = idEquipe;
            }
            console.log(donnees);
            appelAPI("POST", apiURL + "reservations.php", donnees)
                .then(data => {
                    console.log(data);
                    $('.tiny.modal.confirmation')
                        .modal({
                            blurring: true,
                            onDeny : function() {
                                window.location.href = 'index.php?page=listeReservations';
                            },
                            onApprove : function() {
                                var e = document.getElementById("confirm");
                                e.innerHTML = `Confirmer la réservation`;
                                $('.dropdown').dropdown('clear');
                            }
                        })
                        .modal('show');

                })
        }

        // Vérifie si une réservation existe pour l'invité séléctionné
        function verifSiDejaReservation() {
            listeHeber.innerHTML = "";
            if (type != null) {
                main.insertBefore(chargement, main.children[4]);
            }

            var donnees = {"idVIPReser" : id};
            appelAPI("GET", apiURL + "reservations.php", donnees)
                .then(data => {
                    var some = jQuery.parseJSON(data);
                    if (some.length != 0) {
                        console.log("Déja une réservation");
                        if (chargement != null) {
                            chargement.remove();
                        }
                        afficherMessage("Cet invité a déjà une réservation.");
                    } else {
                        search();
                    }
                })
        }


    </script>

    <!-- Modal pour la déconnexion -->
    <div class="ui tiny modal deconnexion" style="position: relative; height: 200px;;">
        <div class="header">
            Déconnexion
        </div>
        <div class="content">
            <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
        </div>
        <div class="actions">
            <div class="ui cancel button">
                Annuler
            </div>
            <div class="ui ok red button">
                Me déconnecter
            </div>
        </div>
    </div>

    <!-- Modal pour la réservation -->
    <div class="ui tiny modal reserver" style="position: relative; height: 220px;">
        <div class="header">
            Réservation
        </div>
        <div class="content">
            <p style="font-size: 1.35em" id="confirmationLabel"><!-- Texte de confirmation  --></p>
        </div>
        <div class="actions">
            <div class="ui cancel button">
                Annuler
            </div>
            <div class="ui ok green button" id="confirm">
                Confirmer la réservation
            </div>
        </div>
    </div>

    <!-- Modal pour la confirmation -->
    <div class="ui tiny modal confirmation" style="position: relative; height: 370px;">
        <div class="header">
            Réservation enregistrée
        </div>
        <div class="content justify-content-center">
            <img src="assets/images/Success.svg" alt="" class="d-block col-5 mx-auto">
            <p style="font-size: 1.30em" class="text-center mx-auto mt-3">La réservation a bien été enregistrée</p>
        </div>
        <div class="actions">
            <div class="ui cancel button">
                Retour calendrier
            </div>
            <div class="ui ok yellow button">
                Nouvelle réservation
            </div>
        </div>
    </div>

</body>