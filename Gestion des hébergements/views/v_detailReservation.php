<?php
    $titre="Détail d'une réservation";
	require_once(PATH_VIEWS."header.php");
?>
    <?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
    <?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>
    <script src="functions/fonctions.js"></script>
</head>

<body style="background-color: #F3F3F3;">
    <div class="container">

        <?php 
            // Affiche le haut de la page avec le nom et prénom de l'utilisateur
            $boutonRetour = true; 
            require_once(PATH_VIEWS."hautPage.php");
        ?>

        <!-- Titre -->
        <div class="row mt-3">
            <p class="text-center col mt-5" style="font-weight: bold; font-size: 1.6em;">Informations de la réservation pour <?= $vip->get_nom_VIP().' '.$vip->get_prenom_VIP() ?></p>
        </div>

        <!-- Bouton modifier et supprimer -->
        <div class="row my-4 justify-content-center">
              <button class="ui right labeled icon button red ml-md-3 ml-0 my-md-0 my-2" onclick="supprimer(<?php echo $reservation->get_id_Reservation()?>)">
                <i class="trash icon"></i>
                Supprimer cette réservation
              </button>
        </div>

        <!--Affichage de l'hébergement-->
        <div class="row bg-white rounded row col-lg-5 col-10 mx-auto">
                <h2 class="mx-auto col-12 mt-2 text-center">Informations sur l'hébergement</h2>
                    
                <table class="table table-bordered col-lg-7 col-9 ml-4">
                    <tr>
                        <td class="font-weight-bold">Nom de l'hébergement</td>

                    </tr>

                    <tr>
                        <td><?=$donnee["nomHebergement"]?></td>


                    </tr>

                    
                </table>
            
                <div class="mt-5 pl-lg-5 pl-0 col">
                    <button class="ui green animated button m-lg-0 mx-auto justify-content-right" tabindex="0" onclick="document.getElementById('formHeber').submit()">
                        <div class="visible content">Voir plus</div>
                            <div class="hidden content">
                                <i class="fas fa-home"></i></i>
                            </div>
                    </button>
                </div>

                <form method="post" id="formHeber" action="index.php?page=detailHebergement">
                    <input type="hidden" name="idHebergement" value='<?php echo $reservation->get_id_Hebergement()?>'>
                </form>
        </div>


            <!--Affichage des coordonnées du VIP-->
            <div class="row bg-white rounded row col-lg-5 col-10 mx-auto mt-4">
                <h2 class="mx-auto col-12 mt-2 text-center">Informations sur le VIP</h2>
                    
                <table class="table table-bordered col-9 mx-auto w-50">
                    <tr>
                        <td class="font-weight-bold">Nom</td>
                        <td><?=$vip->get_nom_VIP()?></td>
                    </tr>
    
                    <tr>
                        <td class="font-weight-bold">Prénom</td>
                        <td><?=$vip->get_prenom_VIP()?></td>
                        
    
                    </tr>

                    <tr>
                        <td class="font-weight-bold">Profession</td>
                        <td><?=$vip->get_profession_VIP()?></td>

                    </tr>

                    <!--Si une personne appartient au staff ou au jury, on affiche son numéro-->
                    <?php
                        if($vip->get_groupe_VIP()!=null)
                        {?>
                            <td class="font-weight-bold">Numéro de jury</td>
                            <td><?=$vip->get_groupe_VIP()?></td>
                        <?php
                        }
                        else if ($vip->get_equipe_VIP()!=null)
                        {?>
                            <td class="font-weight-bold">Numéro d'équipe</td>
                            <td><?=$vip->get_equipe_VIP()?></td>

                        <?php
                        }
                    ?>
    
                        
                </table>



            
            
                    </div>



        
        
        <!-- Pied de page -->
        <footer class="row mt-3">
            <p class="text-center col mb-3" style="font-size: 1em;">Baptiste Faure, Camélia Méraoui - Projet Cannes IUT Lyon 1</p>
        </footer>
    </div>

</body>
<script>
    /* Affiche la fenêtre modale lors de l'appel de cette fonction */
    function deconnexion() {
        $('.tiny.modal.deco')
        .modal({
            blurring: true,
            onApprove : function() {
                window.location.href = 'index.php?deconnexion=true';
            }
        })
        .modal('show')
    }

    function supprimer(idR) {
        $('.tiny.modal.supprimer')
        .modal({
            blurring: true,
            onApprove : function() {
                var e = document.getElementById("delete");
                e.innerHTML = `Chargement ...`;

                var donnees = {"idSupp" : idR};
                appelAPI("POST", apiURL + "reservations.php", donnees)
                    .then(data => {
                        console.log(data);
                        $('.tiny.modal.confirmation')
                        .modal({
                            closable: false,
                            blurring: true,
                            onApprove : function() {
                                window.location.href = 'index.php?page=ajoutReservations';
                                var e = document.getElementById("delete");
                                e.innerHTML = `Supprimer`;
                            },
                            onDeny : function() {
                                window.location.href = 'index.php?page=listeReservations';
                                var e = document.getElementById("delete");
                                e.innerHTML = `Supprimer`;
                            }
                        })
                        .modal('show')
                    })
            }
        })
        .modal('show')
        
    }


</script>

<!-- Modal pour la déconnexion -->
<div class="ui tiny modal deco" style="position: relative; height: 200px;;">
    <div class="header">
        Déconnexion
    </div>
    <div class="content">
        <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
    </div>
    <div class="actions">
        <div class="ui cancel button">
          Annuler
        </div>
        <div class="ui ok red button">
            Me déconnecter
        </div>
    </div>
</div>

<!-- Modal pour la suppression -->
<div class="ui tiny modal supprimer" style="position: relative; height: 200px;;">
    <div class="header">
        Supprimer cet hébergement
    </div>
    <div class="content">
        <p>Êtes-vous sûr de vouloir supprimer cette réservation ? Cette action est irréversible.</p>
    </div>
    <div class="actions">
        <div class="ui cancel button">
          Annuler
        </div>
        <div class="ui ok red button" id="delete">
            Supprimer
        </div>
    </div>
</div>

<!-- Modal pour la confirmation -->
<div class="ui tiny modal confirmation" style="position: relative; height: 200px;;">
    <div class="header">
        Séléctionnez une action
    </div>
    <div class="content">
        <p>La réservation a bien été supprimée.</p>
    </div>
    <div class="actions">
        <div class="ui cancel button">
            Retour au calendrier
        </div>
        <div class="ui ok grey button">
            Ajouter une nouvelle réservation
        </div>
    </div>
</div>


</html>