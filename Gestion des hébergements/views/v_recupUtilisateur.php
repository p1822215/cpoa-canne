<?php
	// Importation
	require_once("functions/fonctions.php");	

	
	// Récupère le nom de l'utilisateur connecté
	$nom = "";
	$prenom = "";
	$espace = "hébergement";
	if (isset($_COOKIE['idUtilisateur'])) {
		$id = $_COOKIE['idUtilisateur'];
		$idCurrentUtilisateur = $id;
		$donnees = array('idUtilisateur' => $id);
		$utilisteurRecu = appelAPI('GET', API_LINK.'utilisateurs.php', $donnees);
		foreach($utilisteurRecu as $unUtil) {
			$nom = $unUtil['prenomUtilisateur'];
			$prenom = $unUtil['nomUtilisateur'];
			$espace = $unUtil['fonctionUtilisateur'];
		}
	}

?>