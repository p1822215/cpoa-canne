﻿<?php
	$titre="Modifier un hébergement";
	$stylesArray=array("ajoutHebergements");
	require_once(PATH_VIEWS."header.php");
?>

<?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
<?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>

<script src="functions/fonctions.js"></script>

</head>

<body style="background-color: #F3F3F3;">
    <div class="container">
        <!-- Header -->
        <?php 
            
            $boutonRetour = true;
            require_once(PATH_VIEWS."hautPage.php");
        ?>

        <form class="ui form row container" action="#" method=""
        enctype="multipart/form-data" id="formulaire" style="font-size: 1.2em;">
            <h3 class="ui dividing header">Modification d'un hébergement</h3>
            
            <!-- Nom de l'hébergement -->
            <div class="row">
                <label class="col-12 font-weight-bold">Nom de l'hébergement</label>
                <input type="text" name="nom" class="ml-3 form-control border border-white col-md-4 col-12 shadow mt-2" placeholder="Nom*" <?php echo "value='".$hebergement->get_nom_Hebergement()."'" ?>required>
            </div>

            <!-- Adresse -->
            <div class="row justify-content-around">
                <label class="col-12 font-weight-bold mt-4">Location</label>
                <input type="text" name="adresse" class="ml-4 ml-md-0 form-control border border-white col-md-4 col-12 shadow mt-2 mt-lg-1" placeholder="Adresse*" <?php echo "value='".$hebergement->get_adresse()."'" ?> required>
                <input type="text" name="ville" class="ml-4 ml-md-0 form-control border border-white col-md-4 col-12 shadow mt-2 mt-lg-1" placeholder="Ville*" <?php echo "value='".$hebergement->get_ville()."'" ?> required>
                <input type="text" name="cp" class="ml-4 ml-md-0 form-control border border-white col-md-3 col-12 shadow mt-2 mt-lg-1" placeholder="Code postal*" <?php echo "value='".$hebergement->get_cp()."'" ?> required>
            </div>

            <!-- Combo box pour les types et les services -->
            <div class="row justify-content-between">
                <label class="col-12 font-weight-bold mt-4">Informations</label>
                
                <select name="type" class="ui fluid dropdown ml-3 shadow border border-white col-12 col-lg-5 mt-2 mt-sm-0">
                    <option value="">Type*</option>
                    <?php
                        foreach($listeTypes as $unType)
                        {
                        ?>
                            <option value=<?= '"'.$unType['idTypeHebergement'].'"';  ?> <?php if($hebergement->get_type() == $unType['libelleType']){echo "selected";}?>> <?= $unType['libelleType']  ?>  </option> 
						<?php
                        }
                    ?>
                    
                </select>

                <select name="services" id="services" multiple="" class="ui fluid search dropdown2 ml-3 shadow border border-white col-12 col-lg-5 mt-3 mt-lg-0">
                    <option value="">Service(s)*</option>
                    <?php
                        foreach($listeServices as $unService)
                        {
                        ?>
                            <option value=<?= '"'.$unService['idService'].'"'; ?> <?php if (in_array($unService['libelleService'],$listeServicesRecus)){echo "selected";}?>><?= $unService['libelleService']?></option> 
						<?php
                        }
                    ?>
                </select>
            </div>

            <!-- Nombre de places -->
            <div class="row">
                <label class="col-12 font-weight-bold mt-4">Nombre de places maximum disponibles</label>
                    <input type="number" name="nbPlaces" class="ml-3 form-control border border-white col-md-3 col-12 shadow mt-2 mt-lg-1" placeholder="Nombre de places*" <?php echo "value='".$hebergement->get_nb_places_total()."'" ?> required> 
            </div>

            <!--Images-->
            <div class="row mt-2">
                
                <label class="col-12 font-weight-bold mt-4">Illustration de l'hébergement</label>

                <?php
                if($hebergement->get_image() == "assets/images/null")
                {
                ?>
                    <p class="ml-3">Vous n'avez pas choisi d'image</p>
                <?php
				}
                else
                {
                ?>
                    <p class="col-12">Vous avez choisi l'image : <?php echo $hebergement->get_image();?></p>
                    

                    <label class="ml-3">Voulez vous supprimer cette image ?</label>
                    <input class="mt-1 ml-2" type="checkbox" id="confsupp" name="confsupp">
                <?php
				}
                ?>
                <label class="col-12 mt-2">Modification de votre image : </label>
                <input id="image" name="image" class="ml-3 shadow" type="file" class="col-5">
            </div>

            <!--Commentaires-->
            <div class="row">
                <label class="col-12 font-weight-bold mt-3">Commentaires</label>
                <div class="field col-12">
                    <textarea name="commentaire" placeholder="Commentaires" class="shadow" rows="2" ><?php echo $hebergement->get_infoComplementaire() ?></textarea>
                </div>
                    

            </div>

            <p class="text-center">*Champs obligatoires</p>

            <div class="row mt-4 mx-auto justify-content-center">
            <button type="submit" class="ui vertical green animated button ml-2 col-8 col-md-5 col-lg-3 mb-5" tabindex="0">
                <div class="visible content">Modifier l'hébergement</div>
                <div class="hidden content">
                    <i class="fas fa-plus-square"></i>
                </div>
            </button>
            </div>
        </form>
        

    
    </div>
    <script>

        var currentImagePath = "<?php echo $hebergement->get_image(); ?>";
        var currentId = "<?php echo $idHebergementRecu ?>";
        var selectedServices = [];

        $('.ui.dropdown').dropdown();
        $('.ui.dropdown2')
        .dropdown({
			  onChange: function(value, text, $selectedItem) {
                selectedServices = value;
    		}
		  });


        /* Affiche la fenêtre modale lors de l'appel de cette fonction */
        function deconnexion() {
            $('.tiny.modal')
            .modal({
                blurring: true,
                onApprove : function() {
                    window.location.href = 'index.php?deconnexion=true';
                }
            })
            .modal('show')
        }

        var form = document.getElementById("formulaire");
        form.addEventListener("submit", ajoutHebergement, true);

        var fileName = null;
        var fileType = null;
        var file = null;
        var changeImage = false

        const fileSelector = document.getElementById('image');
        fileSelector.addEventListener('change', (event) => {
            const fileList = event.target.files;
            fileName = fileList[0].name;
            fileType = fileList[0].type;
            file = fileList[0];
            changeImage = true;
        });

        function ajoutHebergement() {
            event.preventDefault();

            // Recupère les éméments du formulaire
            var inputs = form.elements;
            var nom = inputs["nom"].value;
            var adresse = inputs["adresse"].value;
            var ville = inputs["ville"].value;
            var cp = inputs["cp"].value;
            var type = inputs["type"].value;
            var nbPlace = inputs["nbPlaces"].value;
            var commentaire = inputs["commentaire"].value;

            // récupération des services 
            selectedServices = getSelectedOptions(document.getElementById('services'));

            // Creation d'un tableau
            var donnees = {
                "idHeber" : currentId,
                "update" : "true",
                "nom" : nom,
                "adresse" : adresse,
                "ville" : ville,
                "cp" : cp,
                "type" : type,
                "nbPlaces" : nbPlace,
                "commentaire" : commentaire,
                "idUtilisateur" : <?php echo $idCurrentUtilisateur ?>
            };

            var checkBox = document.getElementById('confsupp');
            if (checkBox != null && checkBox.checked) {
                donnees.imageHeber = "assets/imageHebergements/null";
                appelAPI("POST", "index.php?page=gestionImage", {"pathImage":currentImagePath})
                    .then(data => {
                        console.log("Response :");
                        console.log(data);
                        
                        appelAPI("POST", apiURL + "hebergements.php", donnees)
                            .then(data => {
                                console.log("données edited");
                                console.log(data);
                                var donnees2 = {
                                    "idHebergement" : currentId
                                };

                            appelAPI("GET", apiURL + "hebergements.php", donnees2)
                                .then(data => {
                                    var some = jQuery.parseJSON(data);
                                    var ar = [];
                                    ar.push(some[0]["idHebergement"]);
                                    var idHebergementCree = ar[0];
                                    console.log(idHebergementCree);  
                                    var donnees4 = {
                                            "idToDelete" : idHebergementCree
                                        };
                                    appelAPI("POST", apiURL + "services.php", donnees4)
                                            .then(data => {
                                                for (const idService of selectedServices) {
                                                    console.log(idService);

                                                    // Creation d'un tableau
                                                    var donnees = {
                                                        "idService" : idService,
                                                        "idHebergement" : idHebergementCree
                                                    };

                                                    appelAPI("POST", apiURL + "services.php", donnees)
                                                        .then(data => {
                                                            window.location.href = 'index.php?page=listeHebergements';
                                                        })
                                                }
                                            })
                                })
                        })
                    })
            } else {
                if(file =! null) {
                    donnees.imageHeber = "assets/imageHebergements/" + fileName;
                    appelAPI("POST", "index.php?page=gestionImage", {"pathImage":currentImagePath})
                        .then(data => {
                            console.log("Response :");
                            console.log(data);
                            
                            appelAPI("POST", apiURL + "hebergements.php", donnees)
                                .then(data => {
                                    console.log("données edited");
                                    console.log(data);
                                    var donnees2 = {
                                        "idHebergement" : currentId
                                    };

                                appelAPI("GET", apiURL + "hebergements.php", donnees2)
                                    .then(data => {
                                        var some = jQuery.parseJSON(data);
                                        var ar = [];
                                        ar.push(some[0]["idHebergement"]);
                                        var idHebergementCree = ar[0];
                                        console.log(idHebergementCree);  
                                        var donnees4 = {
                                            "idToDelete" : idHebergementCree
                                        };
                                        appelAPI("POST", apiURL + "services.php", donnees4)
                                            .then(data => {
                                                for (const idService of selectedServices) {
                                            console.log(idService);

                                            // Creation d'un tableau
                                            var donnees = {
                                                "idService" : idService,
                                                "idHebergement" : idHebergementCree
                                            };

                                            appelAPI("POST", apiURL + "services.php", donnees)
                                                .then(data => {
                                                    const files = document.querySelector('[type=file]').files
                                                    const formData = new FormData()

                                                    for (let i = 0; i < files.length; i++) {
                                                        let file = files[i]
                                                        formData.append('files[]', file)
                                                    }

                                                    $.ajax({
                                                            url: "index.php?page=gestionImage",
                                                            dataType: 'script',
                                                            cache: false,
                                                            contentType: false,
                                                            processData: false,
                                                            data: formData,
                                                            type: 'post',
                                                            success: function(data) {
                                                                    console.log(data);
                                                                    console.log("Response :");
                                                                    window.location.href = 'index.php?page=listeHebergements';
                                                            },
                                                    });
                                                })
                                        }
                                            })
                                    })
                            })
                        })
                } else {
                    donnees.imageHeber = "<?php echo $hebergement->get_image()?>";
                    appelAPI("POST", apiURL + "hebergements.php", donnees)
                        .then(data => {
                            console.log("données edited");
                            console.log(data);

                            var donnees2 = {
                                "idHebergement" : currentId
                            };

                        appelAPI("GET", apiURL + "hebergements.php", donnees2)
                            .then(data => {
                                var some = jQuery.parseJSON(data);
                                var ar = [];
                                ar.push(some[0]["idHebergement"]);
                                var idHebergementCree = ar[0];
                                console.log(idHebergementCree);  
                                var donnees4 = {
                                        "idToDelete" : idHebergementCree
                                    };
                                appelAPI("POST", apiURL + "services.php", donnees4)
                                        .then(data => {
                                            for (const idService of selectedServices) {
                                                console.log(idService);

                                                // Creation d'un tableau
                                                var donnees = {
                                                    "idService" : idService,
                                                    "idHebergement" : idHebergementCree
                                                };

                                                appelAPI("POST", apiURL + "services.php", donnees)
                                                    .then(data => {
                                                        window.location.href = 'index.php?page=listeHebergements';
                                                    })
                                            }
                                        })
                            })
                    })
                }
            } 

            console.log(donnees);

            /*appelAPI("POST", apiURL + "hebergements.php", donnees)
                .then(data => {

                    var donnees2 = {
                        "nomHeber" : nom
                    };

                appelAPI("GET", apiURL + "hebergements.php", donnees2)
                    .then(data => {
                        var some = jQuery.parseJSON(data);
                        var ar = [];
                        ar.push(some[0]["idHebergement"]);
                        var idHebergementCree = ar[0];
                        console.log(idHebergementCree);  

                        for (const idService of selectedServices) {
                            console.log(idService);

                            // Creation d'un tableau
                            var donnees = {
                                "idService" : idService,
                                "idHebergement" : idHebergementCree
                            };

                            appelAPI("POST", apiURL + "services.php", donnees)
                                .then(data => {
                                    const files = document.querySelector('[type=file]').files
                                    const formData = new FormData()

                                    for (let i = 0; i < files.length; i++) {
                                        let file = files[i]
                                        formData.append('files[]', file)
                                    }

                                
                                    $.ajax({
                                            url: "index.php?page=gestionImage",
                                            dataType: 'script',
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            data: formData,
                                            type: 'post',
                                            success: function(data) {
                                                    console.log(data);
                                                    console.log("Response :");
                                                    window.location.href = 'index.php?page=listeHebergements';
                                            },
                                    });
                                })

                        }
                    })
            })*/



        };

        // Pour récupérer les services séléctionné
        function getSelectedOptions(sel) {
            var opts = [], opt;
            for (var i=0, len=sel.options.length; i<len; i++) {
                opt = sel.options[i];
                
                if ( opt.selected ) {
                    opts.push(opt.value);
                }
            }
            return opts;
        }
        </script>

        <!-- Modal pour la déconnexion -->
        <div class="ui tiny modal" style="position: relative; height: 200px;;">
            <div class="header">
                D�connexion
            </div>
            <div class="content">
                <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
            </div>
            <div class="actions">
                <div class="ui cancel button">
                  Annuler
                </div>
                <div class="ui ok red button">
                    Me déconnecter
                </div>
            </div>
        </div>

    </script>
    

</body>
</html>