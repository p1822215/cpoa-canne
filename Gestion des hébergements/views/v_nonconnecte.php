<!--Page qui s'affiche lorsque les cookies sont expirés/n'existe pas-->
<?php
    $titre = "Erreur";
	require_once(PATH_VIEWS."header.php");
    echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';
    echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';
    require_once(PATH_VIEWS."recupUtilisateur.php");
?>

<header class="row justify-content-start mt-3 align-items-center">
    <div class="col-lg-2 col-4 m-0 p-0 mx-auto mt-lg-0 mt-3">
        <?php echo '<img src="'.PATH_LOGO.'" alt="" class="d-block col-11 ml-lg-auto ml-0">';?>
    </div>
    <div class="row align-items-center m-0 p-0 ml-lg-3 ml-0 my-lg-0 my-3 col-lg col-12 text-lg-left text-center">
        <p class="col-12 m-0 p-0" style="font-size: 1.9em; color: black; font-weight: bold;">Interface de gestion des <?php if($espace=="Gerant"){echo "hébergements";} else {echo "réservations";}?></p>
        <p class="col-12 m-0 p-0" style="font-size: 1.3em; color: gray;">Espace <?php echo $espace?> - <?php echo $nom?> <?php echo $prenom?></p>
    </div>
    
</header>

<div class="alert alert-danger w-75 mt-5 mx-auto">
	<h2>Vous n'êtes pas/plus connecté. Veuillez vous reconnecter</h2>
</div>

<div class="row mt-5 pl-lg-5 pl-0">
    <button class="ui red animated button m-lg-0 mx-auto" tabindex="0" onclick="window.location.href = 'index.php';">
        <div class="visible content">Retour à la page de connexion</div>
             <div class="hidden content">
                 <i class="arrow left icon"></i>
             </div>
    </button>
</div>

</html>