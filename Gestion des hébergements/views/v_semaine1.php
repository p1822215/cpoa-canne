<?php
    //On récupère la date et on lui change son format
    $dateTab = explode("-",$dateF);
    $dateString = $dateTab[1]."/".$dateTab[2]."/".$dateTab[0];
    $dat = $dateTab[2]."/".$dateTab[1]."/".$dateTab[0];
?>


<div class="calendrier col-12 mt-5">
    <div class="row justify-content-center">
        <h3><span class="">Semaine du <?php echo $dat?></span><a href="index.php?page=listeReservations&semaine=2"> > </a></h3>
    </div>
        
    <table id="calendrier" class="table table-bordered mt-4"> 
        <tr>
            <!--Affichage des dates -->
            <?php
                //On affiche la date +i jours
                for($i=0;$i<7;$i++)
                {
                    $dat = strtotime($dateString."+".$i."days");
                    $datStr = date('d/m/Y', $dat);
                ?>
                    <th><?php echo $datStr?></th>
                <?php
                }
            ?>
        </tr>
        
                                
    </table>
</div>