<?php
	$titre="Ajouter un hébergement";
	$stylesArray=array("ajoutHebergements");
	require_once(PATH_VIEWS."header.php");
?>

<?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
<?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>

<script src="functions/fonctions.js"></script>

</head>

<body style="background-color: #F3F3F3;">
    <div class="container">
        <!-- Header -->
        <?php 
           
            $boutonRetour = true;
            require_once(PATH_VIEWS."hautPage.php");
        ?>

        <form class="ui form row container" action="#" method=""
        enctype="multipart/form-data" id="formulaire">
            <h3 class="ui dividing header">Ajout d'un nouvel hébergement</h3>
            
            <!-- Nom de l'hébergement -->
            <div class="row">
                <label class="col-12 font-weight-bold">Nom de l'hébergement</label>
                <input type="text" name="nom" class="ml-3 form-control border border-white col-md-4 col-12 shadow mt-2" placeholder="Nom*" required>
            </div>

            <!-- Adresse -->
            <div class="row justify-content-around">
                <label class="col-12 font-weight-bold mt-4">Location</label>
                <input type="text" name="adresse" class="ml-4 ml-md-0 form-control border border-white col-md-4 col-12 shadow mt-2 mt-lg-1" placeholder="Adresse*" required>
                <input type="text" name="ville" class="ml-4 ml-md-0 form-control border border-white col-md-4 col-12 shadow mt-2 mt-lg-1" placeholder="Ville*" required>
                <input type="text" name="cp" class="ml-4 ml-md-0 form-control border border-white col-md-3 col-12 shadow mt-2 mt-lg-1" placeholder="Code postal*" required>
            </div>

            <!-- Combo box pour les types et les services -->
            <div class="row justify-content-between">
                <label class="col-12 font-weight-bold mt-4">Informations</label>
                
                <select name="type" required class="ui fluid dropdown ml-3 shadow border border-white col-12 col-lg-5 mt-2 mt-sm-0">
                    <option value="">Type*</option>
                    <?php
                        foreach($listeTypes as $unType)
                        {
                        ?>
                            <option value=<?= '"'.$unType['idTypeHebergement'].'"'; ?>><?= $unType['libelleType']  ?></option> 
						<?php
                        }
                    ?>
                    
                </select>

                <select name="services" multiple="" required class="ui fluid search dropdown2 ml-3 shadow border border-white col-12 col-lg-5 mt-3 mt-lg-0">
                    <option value="">Service(s)*</option>
                    <?php
                        foreach($listeServices as $unService)
                        {
                        ?>
                            <option value=<?= '"'.$unService['idService'].'"'; ?>><?= $unService['libelleService']  ?></option> 
						<?php
                        }
                    ?>
                </select>
            </div>

            <!-- Nombre de places -->
            <div class="row">
                <label class="col-12 font-weight-bold mt-4">Nombre de places maximum disponibles</label>
                    <input type="number" name="nbPlaces" class="ml-3 form-control border border-white col-md-3 col-12 shadow mt-2 mt-lg-1" placeholder="Nombre de places*" required>
            </div>

            <div class="row">
                <label class="col-12 font-weight-bold mt-4">Illustration de l'hébergement</label>
                <input id="image" name="image" class="ml-3 shadow" type="file" class="col-5" accept="image/*">
            </div>

            <div class="row">
                <label class="col-12 font-weight-bold mt-3">Commentaires</label>
                <div class="field col-12">
                    <textarea name="commentaire" placeholder="Commentaires" class="shadow" rows="2"></textarea>
                </div>
                    

            </div>

            <p class="text-center">*Champs obligatoires</p>

            <div class="row mt-4 mx-auto justify-content-center">
            <button type="submit" class="ui vertical green animated button ml-2 col-8 col-md-5 col-lg-3 " tabindex="0">
                <div class="visible content">Ajouter un hébergement</div>
                <div class="hidden content">
                    <i class="fas fa-plus-square"></i>
                </div>
            </button>
            </div>
        </form>
        

    
    </div>
    <script>


        var selectedServices = [];

        $('.ui.dropdown').dropdown();
        $('.ui.dropdown2')
        .dropdown({
			onChange: function(value, text, $selectedItem) {
                selectedServices = value;
    		}
        });
        /* Affiche la fenêtre modale lors de l'appel de cette fonction */
        function deconnexion() {
            $('.tiny.modal')
            .modal({
                blurring: true,
                onApprove : function() {
                    window.location.href = 'index.php?deconnexion=true';
                }
            })
            .modal('show')
        }

        var form = document.getElementById("formulaire");
        form.addEventListener("submit", ajoutHebergement, true);

        var fileName = null;
        var fileType = null;
        var file = null;

        const fileSelector = document.getElementById('image');
        fileSelector.addEventListener('change', (event) => {
            const fileList = event.target.files;
            fileName = fileList[0].name;
            fileType = fileList[0].type;
            console.log(fileList[0]);
            console.log(fileName);
            file = fileList[0];
        });

        function ajoutHebergement() {
            event.preventDefault();

            // Recupère les éméments du formulaire
            var inputs = form.elements;
            var nom = inputs["nom"].value;
            var adresse = inputs["adresse"].value;
            var ville = inputs["ville"].value;
            var cp = inputs["cp"].value;
            var type = inputs["type"].value;
            var nbPlace = inputs["nbPlaces"].value;
            var commentaire = inputs["commentaire"].value;

            // Creation d'un tableau
            var donnees = {
                "nom" : nom,
                "adresse" : adresse,
                "ville" : ville,
                "cp" : cp,
                "type" : type,
                "nbPlaces" : nbPlace,
                "commentaire" : commentaire,
                "imageHeber" : "assets/imageHebergements/" + fileName,
                "idUtilisateur" : <?php echo $idCurrentUtilisateur ?>
            };

            appelAPI("POST", apiURL + "hebergements.php", donnees)
                .then(data => {

                    var donnees2 = {
                        "nomHeber" : nom
                    };

                appelAPI("GET", apiURL + "hebergements.php", donnees2)
                    .then(data => {
                        var some = jQuery.parseJSON(data);
                        var ar = [];
                        ar.push(some[0]["idHebergement"]);
                        var idHebergementCree = ar[0];
                        console.log(idHebergementCree);  

                        for (const idService of selectedServices) {
                            console.log(idService);

                            // Creation d'un tableau
                            var donnees = {
                                "idService" : idService,
                                "idHebergement" : idHebergementCree
                            };

                            appelAPI("POST", apiURL + "services.php", donnees)
                                .then(data => {
                                    const files = document.querySelector('[type=file]').files
                                    const formData = new FormData()

                                    for (let i = 0; i < files.length; i++) {
                                        let file = files[i]
                                        formData.append('files[]', file)
                                    }

                                
                                    $.ajax({
                                            url: "index.php?page=gestionImage",
                                            dataType: 'script',
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            data: formData,
                                            type: 'post',
                                            success: function(data) {
                                                    console.log(data);
                                                    console.log("Response :");
                                                    window.location.href = 'index.php?page=listeHebergements';
                                            },
                                    });
                                })
                        }
                    })
            })
        };
        </script>

        <!-- Modal pour la déconnexion -->
        <div class="ui tiny modal" style="position: relative; height: 200px;;">
            <div class="header">
                Déconnexion
            </div>
            <div class="content">
                <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
            </div>
            <div class="actions">
                <div class="ui cancel button">
                  Annuler
                </div>
                <div class="ui ok red button">
                    Me déconnecter
                </div>
            </div>
        </div>

    </script>
    

</body>
</html>