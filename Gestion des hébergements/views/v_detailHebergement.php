    
    <?php echo '<link rel="stylesheet" href="'.PATH_ASSETS.'semantic/semantic.css">';?>
    <?php echo '<script src="'.PATH_ASSETS.'semantic/semantic.js"></script>';?>
    <script src="functions/fonctions.js"></script>
</head>

<body style="background-color: #F3F3F3;">
    <div class="container">

        <?php 
            // Affiche le haut de la page avec le nom et prénom de l'utilisateur 
            require_once(PATH_VIEWS."hautPage.php");
        ?>

        <!-- Titre -->
        <div class="row mt-3">
            <p class="text-center col mt-5 mb-3" style="font-weight: bold; font-size: 1.6em;">Informations hébergement</p>
        </div>

    
    <?php 
        
        if($_COOKIE['fonction']!="responsable")
		{
            echo '
            <!-- Bouton modifier et supprimer -->
            <div class="row my-4 justify-content-center">
                <button class="ui right labeled icon button green my-md-0 my-2" onclick="modif()">
                    <i class="edit icon"></i>
                    Modifier les informations
                </button>
                <button class="ui right labeled icon button red ml-md-3 ml-0 my-md-0 my-2" onclick="supprimerModal('.$idHebergementRecu.')">
                    <i class="trash icon"></i>
                    Supprimer cet hébergement
                </button>
            </div>
            ';
		}
    ?>

        <!-- Informations hébergement -->
        <div class="row">
            <div class="col-lg-8 col-md-10 col-11 mx-auto m-0 p-0" style="background-color: white; border-radius: 12px;">
                <p class="col text-center my-4" style="font-weight: bold; font-size: 2em;"><?php echo $hebergement->get_nom_Hebergement()?></p>
                <div class="row col-12 m-0 p-0 px-3 pb-3">
                    <div class="col-lg-6 col-12 m-0 p-0 px-3 my-2">
                        <ul class="list-group" style="font-size: 1.15em;">
                            <li class="list-group-item" style="color: orange; font-weight: bold;">Services disponibles</li>
                            <?php 
                                foreach($listeService as $unService) {
                                    echo '<li class="list-group-item">- '.$unService['libelleService'].'</li>';
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-12 m-0 p-0 px-3 my-2">
                        <div class="col-12 m-0 p-0 mb-4">
                            <ul class="list-group" style="font-size: 1.15em;">
                                <li class="list-group-item" style="color: orange; font-weight: bold;">Adresse</li>
                                <li class="list-group-item"><?php echo $hebergement->get_adresse()?>, <?php echo $hebergement->get_ville()?>, <?php echo $hebergement->get_cp()?></li>
                            </ul>
                        </div>
                        <div class="col-12 m-0 p-0 mb-4">
                            <ul class="list-group" style="font-size: 1.15em;">
                                <li class="list-group-item" style="color: orange; font-weight: bold;">Type d'hébergement</li>
                                <li class="list-group-item"><?php echo $hebergement->get_type()?></li>
                            </ul>
                        </div>
                        <div class="col-12 m-0 p-0">
                            <ul class="list-group" style="font-size: 1.15em;">
                                <li class="list-group-item" style="color: orange; font-weight: bold;">Photo</li>
                                <li class="list-group-item m-0 p-1">
                                    <?php 
                                        if ($hebergement->get_image() == "assets/imageHebergements/null") {
                                            echo '<p class="m-2">Aucune image séléctionnée</p>';
                                        } else {
                                            echo '<img src="'.$hebergement->get_image().'" class="col m-0 p-0" alt="">'; 
                                        }
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 m-0 p-0 px-3 mt-4">
                        <div class="col-12 m-0 p-0 mb-4">
                            <ul class="list-group" style="font-size: 1.15em;">
                                <li class="list-group-item" style="color: orange; font-weight: bold;">Informations complémentaires</li>
                                <li class="list-group-item"><?php echo $hebergement->get_infoComplementaire()?></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Places disponibles -->
        <div class="row mt-4 mb-4">
            <div class="col-lg-8 col-md-10 col-11 mx-auto m-0 p-0" style="background-color: white; border-radius: 12px;">
                <div class="row col-12 my-4 ml-3 m-0 p-0">
                    <p class="col-5 text-left m-0 p-l-1" style="font-weight: bold; font-size: 1.5em;">Places disponibles</p>
                    <div class="col"></div>
                    <form method='post' action="index.php?page=detailHebergement">
                         <input type="hidden" value="<?php echo $idPosted; ?>" id="idHebergement" name="idHebergement"> 
                         <button type="submit" class="ui right labeled icon button mr-5" onclick="window.location.reload(false);">
                            <i class="redo alternate icon"></i>
                            Rafraichîr
                            </button>              
                    </form>
                </div>
                
                <div class="row my-4 ml-3">
                    <p class="text-left col-12" style="font-size: 1.3em;">Nombre de places restantes pour la durée du festival : <?php echo $place; ?></p>
                </div>
            </div>
        </div>
        
        <!-- Pied de page -->
        <footer class="row mt-3">
            <p class="text-center col mb-3" style="font-size: 1em;">Baptiste Faure, Camélia Méraoui - Projet Cannes IUT Lyon 1</p>
        </footer>
    </div>
</body>

<script>

var img23 = "<?php echo $imageHebergementRecu;?>">

/* Affiche la fenêtre modale lors de l'appel de cette fonction */
function deconnexion() {
    $('.tiny.modal.deco')
    .modal({
        blurring: true,
        onApprove : function() {
            window.location.href = 'index.php?deconnexion=true';
        }
    })
    .modal('show')
}

function modif()
{
    window.location.href='index.php?page=modifHebergements&id='+<?php echo $idHebergementRecu ?>;
}

/* Affiche la fenêtre modale de suppression lors de l'appel de cette fonction */
function supprimerModal(id) {
    $('.tiny.modal.supprimer')
    .modal({
        blurring: true,
        onApprove : function() {
            supprimerHebergement(id, img23);
        }
    })
    .modal('show')
}

function supprimerHebergement(id, file) {
    console.log(id);
    appelAPI("POST", apiURL + "hebergements.php", {"idToDelete":id})
        .then(data => {
            console.log(data);
            console.log("file : ");
            console.log(file);
            appelAPI("POST", "index.php?page=gestionImage", {"pathImage":file})
                .then(data => {
                    console.log("Response :");
                    console.log(data);
                    window.location.href = 'index.php?page=listeHebergements';
                })
        })
}

</script>

<!-- Modal pour la déconnexion -->
<div class="ui tiny modal deco" style="position: relative; height: 200px;;">
    <div class="header">
        Déconnexion
    </div>
    <div class="content">
        <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
    </div>
    <div class="actions">
        <div class="ui cancel button">
          Annuler
        </div>
        <div class="ui ok red button">
            Me déconnecter
        </div>
    </div>
</div>

<!-- Modal pour la suppression -->
<div class="ui tiny modal supprimer" style="position: relative; height: 200px;;">
    <div class="header">
        Supprimer cet hébergement
    </div>
    <div class="content">
        <p>Êtes-vous sûr de vouloir supprimer cet hébergement ? Cette action est irréversible.</p>
    </div>
    <div class="actions">
        <div class="ui cancel button">
          Annuler
        </div>
        <div class="ui ok red button">
            Supprimer
        </div>
    </div>
</div>


</html>