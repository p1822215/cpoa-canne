<?php
	 session_start();
	 require_once(PATH_FUNCTIONS."fonctions.php");
	 require_once(PATH_CLASSES."Utilisateur.php");
?>

<?php
	$_SESSION = array();
	if(isset($_POST['login']) AND isset($_POST['mdp']))
	{
		$login=htmlspecialchars($_POST['login']);
		$mdp=htmlspecialchars($_POST['mdp']);
		$donnees=array("login" => $login);

		$donneeRecu = appelAPI('GET', API_LINK.'login.php', $donnees); 

		if ($donneeRecu == null) {
			$alert="identifiant inconnu";
        } 
		else
		{
			//On récupère l'utilisateur
			$donnee = $donneeRecu[0];
			$user = new Utilisateur(
			$donnee['idUtilisateur'],
			$donnee['loginUtilisateur'],
			$donnee['mdpUtilisateur'],
			$donnee['nomUtilisateur'],
			$donnee['prenomUtilisateur'],
			$donnee['fonctionUtilisateur']
			);

			//On vérifie le mot de passe et on crée les cookies si le mdp est bon
			if(password_verify($mdp,$user->get_mdp_utilisateur())==true)
			{
				if (ini_get("session.use_cookies")) 
				{ 
					$_SESSION['logged'] = $user->get_id_Utilisateur();
					$_SESSION['fonction'] = $user->get_fonction_utilisateur();
					setcookie("idUtilisateur",$_SESSION['logged'], time()+3600,
					null, null, false, true);
					setcookie("fonction",$_SESSION['fonction'], time()+3600,
					null, null, false, true);
					//Cookie qui va durer une heure

					if($user->get_fonction_utilisateur()=="Gerant")
					{
						header("Location:index.php?page=listeHebergements"); //Aller sur la page des hebergements
					}
					else
					{
						header("Location:index.php?page=listeReservations"); //Aller sur la page des reservations
					}
				}
			}
			else
			{
				$alert="Mauvais mot de passe";
			}
		}
	}

	
    
	session_destroy();
	
	require_once(PATH_VIEWS."login.php");

?>