<?php
	 require_once(PATH_FUNCTIONS."fonctions.php");
	 require_once(PATH_CLASSES."Utilisateur.php");
	 require_once(PATH_CLASSES."Hebergement.php");
	 require_once(PATH_CLASSES."HebergementService.php");
	 
?>

<?php

// Ajoute une image dans le dossier
if (isset($_FILES['files'])) {
    
    $path = "assets/imageHebergements/";
    $all_files = count($_FILES['files']['tmp_name']);

    for ($i = 0; $i < $all_files; $i++) {
        $file_name = $_FILES['files']['name'][$i];
        $file_tmp = $_FILES['files']['tmp_name'][$i];

        $file = $path . $file_name;

        move_uploaded_file($file_tmp, $file);
        echo $file_name.'-----'.$file;
    }

} 
// Supprime une image du dossier
if (isset($_POST['pathImage'])) {
    $nom = $_POST['pathImage'];
    if (!unlink($nom)) {  
        echo ("Une erreur est survenue");  
    }  
    else {  
        echo ("Supprimé !");  
    } 
} else {
    echo "Pas de nom";
}





?>