<?php
	 require_once(PATH_FUNCTIONS."fonctions.php");
	 require_once(PATH_CLASSES."VIP.php");
	 require_once(PATH_CLASSES."Reservation.php");
	 require_once(PATH_VIEWS."recupUtilisateur.php"); //Récupérer le nom et prenom de l'utilisateur
	
	if(isset($_GET['id']))
	{
		$idReser = htmlspecialchars($_GET['id']);
		$tab= array("idReserInv" => $idReser);
		$hebergementsRecu = appelAPI('GET', API_LINK.'reservations.php', $tab);
		

		if(count($hebergementsRecu)!=0)
		{
			//On récupère la réservation ainsi que les coordonnées du VIP
			$donnee = $hebergementsRecu[0];
			$reservation = new Reservation($donnee['idReservation'],
			$donnee['idHebergement'],
			$donnee["idVIP"],
			$donnee["equipeID"],
			$donnee["juryID"]);

			$vip = new VIP($donnee['idVIP'],
			$donnee['nomVIP'],
			$donnee['prenomVIP'],
			$donnee['professionVIP'],
			$donnee['typeVIP'],
			$donnee['groupeJury'],
			$donnee['equipe']
			);

			//Vérification des droits
			if(isset($_COOKIE['idUtilisateur']))
			{
				if($_COOKIE['fonction']=="responsable")
				{
					if(count($hebergementsRecu)!=0)
					{
							
							require_once(PATH_VIEWS."detailReservation.php");
			
					}
					else
					{
						require_once(PATH_VIEWS."404.php");
					}			
				}
				else
				{
					require_once(PATH_VIEWS."nonautorise.php");
				}

			}
			else
			{
				require_once(PATH_VIEWS."nonconnecte.php");
			}

		}
		else
		{
			require_once(PATH_VIEWS."404.php"); //S'affiche si l'id n'est pas dans la BD
		}
				
	}
	else
	{
		require_once(PATH_VIEWS."404.php"); //S'affiche si l'id est nul
	}
	
	
?>