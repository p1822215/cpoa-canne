<?php
    // Titre de la page    
	$titre="Nouvelle réservation";

	// Importation
	require_once(PATH_VIEWS."header.php");
	require_once("functions/fonctions.php");	

	if(isset($_COOKIE['idUtilisateur']))
	{
		if($_COOKIE['fonction']=="responsable")
		{
			require_once(PATH_VIEWS."recupUtilisateur.php");
			// Affiche la vue
			require_once(PATH_VIEWS."ajoutReservations.php");
		}
		else
		{
			require_once(PATH_VIEWS."nonautorise.php");
        }

	}
	else
	{
		require_once(PATH_VIEWS."nonconnecte.php");
	}
	
	

?>