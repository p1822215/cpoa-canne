<?php
    // Titre de la page    
	$titre="Liste des hébergements";

	// Importation
	require_once(PATH_VIEWS."header.php");
	require_once("functions/fonctions.php");	

	if(isset($_COOKIE['idUtilisateur']))
	{
		if($_COOKIE['fonction']=="Gerant")
		{
			require_once(PATH_VIEWS."recupUtilisateur.php");
			// Affiche la vue
			$boutonRetour = false;
			require_once(PATH_VIEWS."listeHebergements.php");
		}
		else
		{
			require_once(PATH_VIEWS."nonautorise.php");
        }

	}
	else
	{
		require_once(PATH_VIEWS."nonconnecte.php");
	}
	
	

?>