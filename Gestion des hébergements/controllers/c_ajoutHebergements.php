<?php
	 require_once(PATH_FUNCTIONS."fonctions.php");
	 require_once(PATH_CLASSES."Utilisateur.php");
	 require_once(PATH_CLASSES."Hebergement.php");
	 require_once(PATH_CLASSES."HebergementService.php");
	 require_once(PATH_VIEWS."recupUtilisateur.php"); 
	 
?>

<?php
	$listeServices = appelAPI('GET', API_LINK.'services.php', null); 
	$listeTypes = appelAPI('GET', API_LINK.'type.php', null); 
	$alert=null;
	
	//Vérification des droits pour accéder à cette page
	if(isset($_COOKIE['idUtilisateur']))
	{
		if($_COOKIE['fonction']=="Gerant")
		{
			
			require_once(PATH_VIEWS."ajoutHebergements.php");
		}
		else 
		{
			require_once(PATH_VIEWS."nonautorise.php");
		}
	}
	else
	{
		require_once(PATH_VIEWS."nonconnecte.php");
	}
?>

<?php
	