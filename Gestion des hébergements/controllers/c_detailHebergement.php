<?php
    // Titre de la page    
	$titre="Détail hébergement";

	// Importation
	require_once(PATH_VIEWS."header.php");
	require_once(PATH_CLASSES."Hebergement.php");
	require_once("functions/fonctions.php");
	require_once(PATH_VIEWS."recupUtilisateur.php");	

	if(isset($_COOKIE['idUtilisateur']))
	{
			// Affichage du bouton retour
			$boutonRetour = false;

			// Récupération de l'hébergement
			if (isset($_POST["idHebergement"])) {
				$idPosted = $_POST["idHebergement"];
				$donneesU = array('idHebergement' => $idPosted);
				$hebergementsRecu = appelAPI('GET', API_LINK.'hebergements.php', $donneesU);
	
				$donnee = $hebergementsRecu[0];

				$idHebergementRecu = $donnee['idHebergement'];
				$nomHebergementRecu = $donnee['nomHebergement'];
				$nbPlaceHebergementRecu = 0;
				$adresseHebergementRecu = $donnee['adresse'];
				$villeHebergementRecu = $donnee['ville'];
				$cpHebergementRecu = $donnee['cp'];
				$idTypeHebergementRecu = $donnee['libelleType'];
				$infoHebergementRecu = $donnee['infoComplementaire'];
				$imageHebergementRecu = $donnee['imageHeber'];
				$idUserHebergementRecu = $donnee['idUtilisateur'];
				$place = $donnee['nbPlaceRestantes'];
		
				$hebergement = new Hebergement($idHebergementRecu,$nomHebergementRecu,$nbPlaceHebergementRecu,$adresseHebergementRecu,$villeHebergementRecu,$cpHebergementRecu,$imageHebergementRecu,$idTypeHebergementRecu,$infoHebergementRecu,$idUserHebergementRecu);

				$donneesS = array('idHebergement' => $idPosted);
				$listeService = appelAPI('GET', API_LINK.'services.php', $donneesS);
				// Affiche la vue
				require_once(PATH_VIEWS."detailHebergement.php");
			} 
			else 
			{
				require_once(PATH_VIEWS."404.php");
				// Une erreur est survenue -> page 404
			}
				
		
		
	}
	else 
	{
		require_once(PATH_VIEWS."nonconnecte.php");
    }

	

?>