﻿<?php
	 require_once(PATH_FUNCTIONS."fonctions.php");
	 require_once(PATH_CLASSES."Utilisateur.php");
	 require_once(PATH_CLASSES."Hebergement.php");
	 require_once(PATH_CLASSES."HebergementService.php");
	 require_once(PATH_VIEWS."recupUtilisateur.php"); //Récupérer le nom et prenom de l'utilisateur<?php
	
	if(isset($_GET['id']))
	{
		$idHebergement = htmlspecialchars($_GET['id']);
		$tab= array("idHebergement" => $idHebergement);
		$hebergementsRecu = appelAPI('GET', API_LINK.'hebergements.php', $tab);
		

		if(count($hebergementsRecu)!=0)
		{
			$donnee = $hebergementsRecu[0];
			$idHebergementRecu = $donnee['idHebergement'];
			$nomHebergementRecu = $donnee['nomHebergement'];
			$nbPlaceHebergementRecu = $donnee['nbPlacesTotal'];
			$adresseHebergementRecu = $donnee['adresse'];
			$villeHebergementRecu = $donnee['ville'];
			$cpHebergementRecu = $donnee['cp'];
			$idTypeHebergementRecu = $donnee['libelleType'];
			$infoHebergementRecu = $donnee['infoComplementaire'];
			$imageHebergementRecu = $donnee['imageHeber'];
			$idUserHebergementRecu = $donnee['idUtilisateur'];
			$hebergement = new Hebergement($idHebergement,$nomHebergementRecu,$nbPlaceHebergementRecu,$adresseHebergementRecu,$villeHebergementRecu,$cpHebergementRecu,$imageHebergementRecu,$idTypeHebergementRecu,$infoHebergementRecu,$idUserHebergementRecu);
			$donneesS = array('idHebergement' => $idHebergementRecu);
			$listeService = appelAPI('GET', API_LINK.'services.php', $donneesS);
			$listeServicesRecus = array();
		
			$listeServicesRecus = array_column($listeService, 'libelleService');
		
		}
		
		
		

		
	}


	$listeServices = appelAPI('GET', API_LINK.'services.php', null); 
	$listeTypes = appelAPI('GET', API_LINK.'type.php', null); 
	$alert=null;
	//Modif d'un hébergement
	
	if(isset($_COOKIE['idUtilisateur']))
	{
		if($_COOKIE['fonction']=="Gerant")
		{
			if(count($hebergementsRecu)!=0)
			{
					if(isset($_FILES['image']['name']))
					{
						if($_FILES['image']['name']!=null)
						{
							if(!preg_match('#[a-zA-Z]+\.(jpeg|png|gif|jpg)#',$_FILES['image']['name']))
							{
								$alert="L'extension de fichier est incorrecte. Extensions accept�es : jpeg,jpg,png,gif";
							}
							else 
							{
								$nomImage = $_FILES['image']['name'];
								move_uploaded_file($_FILES['image']['tmp_name'],PATH_IMAGES_HEBERGEMENT.$_FILES['image']['name']);
			
							}
						}
						else 
						{
							$nomImage = "pasdephoto.jpg";
						}
					}
					else 
					{
						$nomImage = PATH_IMAGES."pasdephoto.jpg";
					}

					if(!isset($_POST['type']))
					{
						$alert = "Le type de l'hébergement n'est pas défini";
					}

					if(!isset($_POST['services']))
					{
						$alert = "Les services proposés ne sont pas définis";
					}

					if(isset($_POST['nom']) OR $alert==null)
					{
						header("Location:index.php?page=listeHebergements");

						$nomHeber = htmlspecialchars($_POST['nom']);
						$adresse = htmlspecialchars($_POST['adresse']);
						$ville = htmlspecialchars($_POST['ville']);
						$cp = htmlspecialchars($_POST['cp']);
						$type = $_POST['type'];
						$tabServices = $_POST['services'];
						$nbPlaces = htmlspecialchars($_POST['nbPlaces']);
						$commentaire = htmlspecialchars($_POST['commentaire']);

		
						$donnees=array(
										   "ajout" => true,
										   "nom" => $nomHeber,
										   "nbPlaces" => $nbPlaces,
										   "adresse" => $adresse,
										   "ville" =>$ville,
										   "cp" => $cp,
										   "nomImage" => $nomImage,
										   "type" => $type[0],
										   "commentaire" => $commentaire,
										   "idUtilisateur" => $_COOKIE['idUtilisateur']);
			
						appelAPI('MODIFIER', API_LINK.'hebergements.php', $donnees);
								

						$nomTab = array("nomHeber" => $nomHeber);
						$response = appelAPI('POST', API_LINK.'hebergements.php', $nomTab);
						$idTab = $response[0];
						$idTab = $idTab['idHebergement'];

						for($i=0;$i<count($tabServices);$i++)
						{
							$nomTab = array(
											"servHeber" => true,
											"id" => $idTab,
											"unService" => $tabServices[$i]);
							appelAPI('POST', API_LINK.'hebergements.php', $nomTab);
						}
					}
					require_once(PATH_VIEWS."modifHebergements.php");
	
			}
			else
			{
				require_once(PATH_VIEWS."404.php");
			}			
		}
		else
		{
			require_once(PATH_VIEWS."nonautorise.php");
        }

	}
	else
	{
		require_once(PATH_VIEWS."nonconnecte.php");
	}


	
	
?>

<?php
	