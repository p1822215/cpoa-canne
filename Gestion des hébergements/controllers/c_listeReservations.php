<?php
	require_once(PATH_FUNCTIONS."fonctions.php");
	require_once(PATH_CLASSES."VIP.php");
	$donneeRecu = appelAPI('GET', API_LINK.'dateDebut.php', null);
	
	if($donneeRecu!=null) 
	{
		$donnee = $donneeRecu[0];
		$dateF = $donnee['dateDeb'];
		if(date("Y-m-d")>$dateF) 
		{
			header("Location:index.php?page=listeReservations");
			$donneeRecu = appelAPI('SUPPRIMER', API_LINK.'dateDebut.php', null);
			//Si la date du festival est antérieure à aujourd'hui, alors celle-ci est automatiquement supprimée
		}
	}
	else
	{
		$dateF = null; //La date du début du festival n'est pas définie
	}

	

?>

<?php
    // Titre de la page    
	$titre="Liste des réservations";

	// Importation
	require_once(PATH_VIEWS."header.php");
	require_once("functions/fonctions.php");	

	//Vérification des droits
	if(isset($_COOKIE['idUtilisateur']))
	{
		if($_COOKIE['fonction']=="responsable")
		{
			require_once(PATH_VIEWS."recupUtilisateur.php");
			// Affiche la vue
			
			require_once(PATH_VIEWS."listeReservations.php");
		}
		else
		{
			require_once(PATH_VIEWS."nonautorise.php");
        }

	}
	else
	{
		require_once(PATH_VIEWS."nonconnecte.php");
	}
	
	

?>