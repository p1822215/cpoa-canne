<?php
	
	// Accès à l'API
	const API_LINK = 'projetcannes.alwaysdata.net/';

	const ROUGE = '#DC143C';
	const GOLD = '#fcba3a';

	//Chemins vers les dossiers du sites
	define("PATH_ASSETS",'./assets/');
	define("PATH_CLASSES",'./classes/');
	define("PATH_CONTROLLERS",'./controllers/c_');
	define("PATH_VIEWS",'./views/v_');
	define("PATH_FUNCTIONS",'./functions/');

	//Chemins des ressources 
	define("PATH_CSS",PATH_ASSETS."stylesheets/s_");
	define("PATH_IMAGES",PATH_ASSETS."images/");
	define("PATH_IMAGES_HEBERGEMENT",PATH_IMAGES."imageHebergements/");

	define("PATH_LOGO",PATH_IMAGES."logo.png");
	define("PATH_HEBERGEMENTS_IMAGES",PATH_IMAGES."hebergements_images/"); 


?>
	