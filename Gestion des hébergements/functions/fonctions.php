<?php

   /* 
      Fonction pour appeler l'API en PHP
   
      Paramètres : 
         - $methode : GET, POST, SUPPRIMER, MODIFIER
         - $url : url de l'api (exemple : projetcannes.alwaysdata.net/produits.php)
         - $donnees : tableau contenant les données à envoyer à l'API
   */

   function appelAPI($methode, $url, $donnees = array(), $donnees2= array()) {
        // Initialisation d'une variable cURL
        $curl = curl_init();
       
        // Choix du type de méthode : POST, GET, SUPPRIMER, MODIFIER
        switch ($methode){
           case "POST":
               curl_setopt($curl, CURLOPT_POST, 1);
               if ($donnees != array())
                  curl_setopt($curl, CURLOPT_POSTFIELDS, $donnees);
               if ($donnees2 != array())
                  curl_setopt($curl, CURLOPT_POSTFIELDS, $donnees2);
               break;

           case "GET":
               if ($donnees != array())
                  $url = sprintf("%s?%s", $url, http_build_query($donnees));
               break;

           case "SUPPRIMER": //Get
               curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "SUPPRIMER");
               if ($donnees != array())
                   $url = sprintf("%s?%s", $url, http_build_query($donnees));	 					
               break;

            case "MODIFIER": //Post
               curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "MODIFIER");
               if ($donnees != array())
                  curl_setopt($curl, CURLOPT_POSTFIELDS, $donnees);			 					
               break;
            
           default: // ?
               if ($donnees != array())
                  $url = sprintf("%s?%s", $url, http_build_query($donnees));
        }

        // Paramètre l'url
        curl_setopt($curl, CURLOPT_URL, $url);

        // Pour récupérer les données de la page
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        // Tous les types de connexions
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

        // Execution
        $result = curl_exec($curl);

        if(!$result) {
            die('<div class="ui compact red message mx-auto text-center my-5">
            <p class="text-center">Une erreur est survenue, veuillez vérifier votre connexion internet et réessayer.</p>
          </div>');
        }

        // Fermeture de la requete
        curl_close($curl);

        // Convertir le JSON
        $donnees = json_decode($result, true);

        // Résultat
        return $donnees;
     }
?>