

var apiURL = 'http://projetcannes.alwaysdata.net/';


/* 
   Fonction pour appeler l'API en Javascript

   Paramètres : 
      - methode : GET, POST, SUPPRIMER, MODIFIER
      - url : url de l'api (exemple : projetcannes.alwaysdata.net/produits.php)
      - donnees : tableau contenant les données à envoyer à l'API (exemple: {"prenom":"Baptiste","nom":"Faure"})
*/
function appelAPI(methode, url, donnees) {
    var urlWithParameters = url;

    return new Promise((resolve, reject) => {
        $.ajax({
            url: urlWithParameters,
            type: methode,
            data: donnees,
            success: function(data) {
                resolve(data)
            },
            error: function(error) {
                reject(error)
            },
        })
    })
}





