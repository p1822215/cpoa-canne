<?php
    class Service
    {
        private $idService;
        private $libelleService;
        
        public function __construct($id,$lib)
        {
            $this->idService = $id;
            $this->libelleService = $lib;
            
        }
        
        public function get_id_service()
        {
            return $idService;
        }
        
        public function get_libelle_service()
        {
            return $libelleService;
        }
        
        public function set_id_service($id)
        {
            $this->idService = $id;
        }
        
        public function set_libelle_service($lib)
        {
            $this->libelleService = $lib;
        }
        
    }
?>