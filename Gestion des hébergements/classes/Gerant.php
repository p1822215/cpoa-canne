<?php
    class Gerant
    {
        private $idGerant;
        private $nomGerant;
        private $prenomGerant;
        
        public function __construct($id,$nom,$prenom)
        {
            $this->idGerant = $id;
            $this->nomGerant = $nom;
            $this->prenomGerant = $prenom;
            
        }
        
        public function get_id_gerant()
        {
            return $idGerant;
        }
        
        public function get_nom_gerant()
        {
            return $nomGerant;
        }
        
        public function get_prenom_gerant()
        {
            return $prenomGerant;
        }
        
        public function set_id_gerant($id)
        {
            $this->idGerant = $id;
        }
        
        public function set_nom_gerant($nom)
        {
            $this->nomGerant = $nom;
        }
        
        public function set_prenom_gerant($prenom)
        {
            $this->prenomGerant = $prenom;
        }
    }
?>