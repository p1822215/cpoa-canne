<?php
    class Utilisateur
    {
        private $idUtilisateur;
        private $login;
        private $mdp;
        private $nomUtilisateur;
        private $prenomUtilisateur;
        private $fonction;
        
        public function __construct($id,$log,$m,$nom,$prenom,$f)
        {
            $this->idUtilisateur = $id;
            $this->login = $log;
            $this->mdp = $m;
            $this->nomUtilisateur = $nom;
            $this->prenomUtilisateur = $prenom;
            $this->fonction = $f;
            
        }
        
        public function get_id_utilisateur()
        {
            return $this->idUtilisateur;
        }
        
        public function get_login_utilisateur()
        {
            return $this->login;
        }
        
        public function get_mdp_utilisateur()
        {
            return $this->mdp;
        }
        
        public function get_nom_utilisateur()
        {
            return $this->nomUtilisateur;
        }
        
        public function get_prenom_utilisateur()
        {
            return $this->prenomUtilisateur;
        }
        
        public function get_fonction_utilisateur()
        {
            return $this->fonction;
        }
        
        public function set_id_utilisateur($id)
        {
            $this->idUtilisateur = $id;
        }
        
        public function set_login_utilisateur($login)
        {
            $this->login = $login;
        }
        
        public function set_mdp_utilisateur($mdp)
        {
            $this->mdp = $mdp;
        }
        
        public function set_nom_utilisateur($nom)
        {
            $this->nomUtilisateur = $nom;
        }
        
        public function set_prenom_utilisateur($prenom)
        {
            $this->prenomUtilisateur = $prenom;
        }
        
        public function set_fonction_utilisateur($fonction)
        {
            $this->fonction = $fonction;
        }
    }
?>