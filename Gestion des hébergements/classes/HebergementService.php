<?php
    class HebergementService
    {
        private $tabServices;
        private $idHebergement;
        
        public function __construct($id,$tab)
        {
            $this->idHebergement= $id;
            $this->tabServices = $tab;
        }
        
        public function get_tab_services()
        {
            return $tabServices;
        }
        
        public function get_id_gerant()
        {
            return $idHebergement;
        }
        
        public function set_tab_services($tab)
        {
            $this->tabServices = $tab;
        }
        
        public function set_id_gerant($id)
        {
            $this->idHebergement = $id;
        }
    }
?>