<?php
    class Responsable
    {
        private $idResponsable;
        private $nomResponsable;
        private $prenomResponsable;
        
        public function __construct($id,$nom,$prenom)
        {
            $this->idResponsable = $id;
            $this->nomResponsable = $nom;
            $this->prenomResponsable = $prenom;
            
        }
        
        public function get_id_responsable()
        {
            return $idResponsable;
        }
        
        public function get_nom_responsable()
        {
            return $nomResponsable;
        }
        
        public function get_prenom_responsable()
        {
            return $prenomResponsable;
        }
        
        public function set_id_responsable($id)
        {
            $this->idResponsable = $id;
        }
        
        public function set_nom_responsable($nom)
        {
            $this->nomResponsable = $nom;
        }
        
        public function set_prenom_responsable($prenom)
        {
            $this->prenomResponsable = $prenom;
        }
    }
?>