<?php
    class Type
    {
        private $idType;
        private $libelleType;
        
        public function __construct($id,$lib)
        {
            $this->idType = $id;
            $this->libelleType = $lib;
            
        }
        
        public function get_id_type()
        {
            return $idType;
        }
        
        public function get_libelle_type()
        {
            return $libelleType;
        }
        
        public function set_id_type($id)
        {
            $this->idType = $id;
        }
        
        public function set_libelle_type($lib)
        {
            $this->libelleType = $lib;
        }
        
    }
?>