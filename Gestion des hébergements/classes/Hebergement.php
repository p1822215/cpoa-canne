<?php

    class Hebergement
    {
        private $idHebergement;
        private $nomHebergement;
        private $nbPlacesTotal;
        private $adresse;
        private $ville;
        private $cp;
        private $image;
        private $type;
        private $infoComplementaire;
        private $idUtilisateur;
        
        
        public function __construct($idHeber,$nom,$nbPlaces,$adr,$v,$cp,$img,$type,$info,$idUser)
        {
            $this->idHebergement = $idHeber;
            $this->nomHebergement = $nom;
            $this->nbPlacesTotal = $nbPlaces;
            $this->adresse = $adr;
            $this->ville=$v;
            $this->cp=$cp;
            $this->image = $img;
            $this->type = $type;
            $this->infoComplementaire = $info;
            $this->idUtilisateur=$idUser;
        }

        public function get_id_Hebergement()
        {
            return $this->idHebergement;
        }
        
        public function get_nom_Hebergement()
        {
            return $this->nomHebergement;
        }

        public function get_nb_places_total()
        {
            return $this->nbPlacesTotal;
        }

        public function get_adresse()
        {
            return $this->adresse;
        }

        public function get_ville()
        {
            return $this->ville;
        }
        
        public function get_cp()
        {
            return $this->cp;
        }

        public function get_type()
        {
            return $this->type;
        }

        public function get_image()
        {
            return $this->image;
        }

        public function get_infoComplementaire()
        {
            return $this->infoComplementaire;
        }

        public function get_id_Utilisateur()
        {
            return $this->idUtilisateur;
        }
        
        public function set_id_hebergement($idHeber)
        {
            $this->idHebergement = $idHeber;
        }
        
        public function set_nom_hebergement($nomHeber)
        {
            $this->nomHebergement = $nomHeber;
        }
        
        public function set_nb_places_total($nbT)
        {
            $this->nbPlacesTotal = $nbT;
        }
        
        public function set_adresse($adr)
        {
            $this->adresse = $adr;
        }

        public function set_ville($v)
        {
            $this->adresse = $v;
        }

        public function set_cp($cp)
        {
            $this->adresse = $cp;
        }

        public function set_type($t)
        {
            $this->type = $t;
        }

        public function set_image($img)
        {
            $this->image = $img;
        }
        
        public function set_infoComplementaire($infoComplementaire)
        {
            $this->infoComplementaire = $infoComplementaire;
        }
        
        public function set_id_Utilisateur($id)
        {
            $this->idUtilisateur = $id;
        }

        public function __toString()
        {
            return $this->nomHebergement . ' '. $this->idHebergement;
        }
    }
?>