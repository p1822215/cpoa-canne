<?php

    class Reservation
    {
        private $idReservation;
        private $idHebergement;
        private $idVIP;
        private $equipeID;
        private $juryID;
        
        public function __construct($idRes,$idHeb,$idV,$e,$j)
        {
            $this->idReservation = $idRes;
            $this->idHebergement = $idHeb;
            $this->idVIP = $idV;
            $this->equipeID = $e;
            $this->juryID = $j;
        }
        
        public function get_id_Reservation()
        {
            return $this->idReservation;
        }
        
        public function get_id_Hebergement()
        {
            return $this->idHebergement;
        }
        
        public function get_id_vip()
        {
            return $this->idVIP;
        }
        
        public function get_equipeID()
        {
            return $this->equipeID;
        }

        public function get_juryID()
        {
            return $this->juryiD;
        }

        
        public function set_id_reservation($idRes)
        {
            $this->idReservation = $idRes;
        }

        public function set_id_hebergement($idHe)
        {
            $this->idHebergement = $idHe;
        }

        public function set_VIP($idV)
        {
            $this->idVIP = $idV;
        }

        public function set_equipeID($e)
        {
            $this->equipeID = $e;
        }

        public function set_juryID($j)
        {
            $this->juryiD = $j;
        }

    }
?>