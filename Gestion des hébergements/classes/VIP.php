<?php

    class VIP
    {
        private $idVIP;
        private $nomVIP;
        private $prenomVIP;
        private $professionVIP;
        private $typeVIP;
        private $groupeJury = null;
        private $equipe = null;
        
        public function __construct($id,$nom,$prenom,$profession,$type,$groupe = null, $equipe = null)
        {
            $this->idVIP = $id;
            $this->nomVIP = $nom;
            $this->prenomVIP = $prenom;
            $this->professionVIP = $profession;
            $this->typeVIP = $type;
            $this->groupeJury = $groupe;
            $this->equipe = $equipe;
        }
        public function get_id_VIP()
        {
            return $this->idVIP;
        }
        public function get_nom_VIP()
        {
            return $this->nomVIP;
        }
        
        public function get_prenom_VIP()
        {
            return $this->prenomVIP;
        }
        public function get_profession_VIP()
        {
            return $this->professionVIP;
        }
        public function get_type_VIP()
        {
            return $this->typeVIP;
        }
        public function get_groupe_VIP()
        {
            return $this->groupeJury;
        }
        public function get_equipe_VIP()
        {
            return $this->equipe;
        }

        
        public function set_id_VIP($id)
        {
            $this->idVIP = $id;
        }

        public function set_nom_VIP($nom)
        {
            $this->nomVIP = $nom;
        }
        public function set_prenom_VIP($prenom)
        {
            $this->prenomVIP = $prenom;
        }
        public function set_profession_VIP($prof)
        {
            $this->professionVIP = $prof;
        }
        public function set_type_VIP($type)
        {
            $this->typeVIP = $type;
        }
        public function set_groupe_jury($groupe)
        {
            $this->groupeJury = $groupe;
        }
        public function set_equipe($eq)
        {
            $this->equipe = $eq;
        }

    }
?>