<?php
	require_once("./config/config.php");

	
	if(!isset($_GET['page']))
	{
		$_SESSION = array();
		setcookie("idUtilisateur","", time()-3600, null, null, false, true);
		setcookie("fonction","", time()-3600, null, null, false, true);
		$controller="login";
	}
	else
	{
		$page = htmlspecialchars($_GET['page']);

		$controller = $page;
		
		/*if(!isset($_SESSION['logged']) AND !isset($_COOKIE['idUtilisateur']))
		{
			$controller = "login";
		}
		else {
			$controller = $page;
		}*/
		
	}

	if(isset($_GET['deconnexion']))
	{
		$_SESSION = array();
		setcookie("idUtilisateur","", time()-3600, null, null, false, true); 
		//Détruire le cookie en cas de déconnexion
        
		header('Location:index.php');
        
	}

	if(is_file(PATH_CONTROLLERS.$controller.".php"))
	{
		require_once(PATH_CONTROLLERS.$controller.".php");
	}
	else 
	{
		require_once(PATH_VIEWS."404.php");
	}

	

?>