<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, SUPPRIMER, MODIFIER, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


	include("db_connect.php");
	$methode = $_SERVER["REQUEST_METHOD"];
	
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
		case 'GET':
			if (isset($_GET["nomSalle"])){
				
				$nom = htmlspecialchars($_GET["nomSalle"]);
				getSalleParSonNom($nom);				
			}
			else
			{
				// On récupère toutes les salles
				getAllSalles();
			}
			
			break;
			
		case 'POST':
			if(isset($_POST["modifNbPlace"]))
			{
				$nomS=htmlspecialchars($_POST['nomSalle']);
				$nbP=htmlspecialchars($_POST['nbPlaces']);
				updateNbPlaceSalle($nomS, $nbP);
			}
			break;

		case 'SUPPRIMER':
			
			
			break;
			
		case 'MODIFIER':
						
			break;
		default:
			break;
	}

    // Récupérer toutes les catégories 
    
	function getAllSalles()
	{
		global $conn;
		$query = "SELECT * FROM Salle";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	// Récupérer une salle par son nom

	function getSalleParSonNom($nomSalle)
	{
		global $conn;
		$query = "SELECT * FROM Salle WHERE nomSalle='".$nomSalle."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response);
	}

	// Modifier nombre de places d'une salle
	function updateNbPlaceSalle($nomS, $nbP)
	{
		global $conn;
		$query = "UPDATE Salle SET nbPlaces = '".$nbP."' WHERE nomSalle = '".$nomS."'";
		mysqli_query($conn, $query);
		echo "Catégorie modifiée !";
	}



	
?>