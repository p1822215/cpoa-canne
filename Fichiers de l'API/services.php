<?php
	// Important : Configuration de l'accès à l'API
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, SUPPRIMER, MODIFIER, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

	include("db_connect.php");

	$methode = $_SERVER["REQUEST_METHOD"];
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
        case 'GET':
            if(isset($_GET["idHebergement"]))
            {
                $idHebergement=htmlspecialchars($_GET['idHebergement']);
				getServiceWithId($idHebergement);
            } else {
				getAllServices();
			}
			break;
		case 'POST':
			// Ajouter un service
			if (isset($_POST["idService"])) {
				$idHebergement=htmlspecialchars($_POST['idHebergement']);
				$idService=htmlspecialchars($_POST['idService']);
				addServices($idHebergement, $idService);
			} else {
				if (isset($_POST["idToDelete"])) {
					$id=htmlspecialchars($_POST['idToDelete']);
					deleteServices($id);
				}
			}
			

			break;
		case 'SUPPRIMER':
			// Supprimer un service - on récupère l'id
			
			break;
		case 'MODIFIER':
			// Modifier un service
			
			break;
		default:
			break;
	}

	/*
		Fonction qui récupère tous les services de la base de données
	*/
	function getAllServices() {
		global $conn;
		$query = "SELECT * FROM Service";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*
		Fonction qui récupère le service correspondant à un id d'hébergement
		Paramètres : 
			- $idHebergement : id de l'hébergement dont on veut récupérer les services
	*/
	function getServiceWithId($idHebergement) {
		global $conn;
        $query = "SELECT s.libelleService, hs.idHebergement FROM `Service` as s, `HebergementService` as hs WHERE hs.idService = s.idService and hs.idHebergement = '".$idHebergement."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	function addServices($idHeb, $idServ) {
		global $conn;
        $query2 = "INSERT INTO HebergementService(idHebergement,idService) values('".$idHeb."','".$idServ."')";
        mysqli_query($conn, $query2);
	}
	
	function deleteServices($id) {
		global $conn;
		$query = "DELETE FROM `HebergementService` WHERE idHebergement = ".$id;
        mysqli_query($conn, $query);
	}


?>