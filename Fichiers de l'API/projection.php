<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, SUPPRIMER, MODIFIER, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


	include("db_connect.php");
	$methode = $_SERVER["REQUEST_METHOD"];
	
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
		case 'GET':
			if ((isset($_GET["creneauProjection"])) && (isset($_GET["idFilm"])) ){
				$creneau = htmlspecialchars($_GET["creneauProjection"]);
				$idF = htmlspecialchars($_GET["idFilm"]);
				recupererUneProjection($creneau, $idF);			
			}
			if((isset($_GET["idDuFilm"])))
			{
				$idFilm = htmlspecialchars($_GET["idDuFilm"]);
				recupererListeProjectionsFilm($idFilm);
			}
			if((isset($_GET["idProjection"])))
			{
				$idP = htmlspecialchars($_GET["idProjection"]);
				recupererSalleProjection($idP);
			}
			break;
			
		case 'POST':
			if(isset($_POST["ajoutProjection"]))
			{
				$dateProjection=htmlspecialchars($_POST['dateProjection']);
				$creneauProjection=htmlspecialchars($_POST['creneauProjection']);
				$idFilm=htmlspecialchars($_POST['idFilm']);
				
				addProjection($dateProjection, $creneauProjection, $idFilm);
			}
			
			if(isset($_POST["ajoutProjectionSalle"]))
			{
				$idSalle=htmlspecialchars($_POST['idSalle']);
				$idProj=htmlspecialchars($_POST['idProjection']);
				addProjectionSalle($idSalle, $idProj);
			}

			if (isset($_POST['modifierProjection'])) {
				$dateProjection=htmlspecialchars($_POST['dateProjection']);
				$creneauProjection=htmlspecialchars($_POST['creneauProjection']);
				$idFilm=htmlspecialchars($_POST['idFilm']);
				$idProjection=htmlspecialchars($_POST['idProjection']);

				modifierProjection($dateProjection, $creneauProjection, $idFilm, $idProjection);
			}

			if (isset($_POST['modifierProjectionSalle'])) {
				$idSalle=htmlspecialchars($_POST['idSalle']);
				$idProj=htmlspecialchars($_POST['idProjection']);

				modifierProjectionSalle($idSalle, $idProj);
			}

			if(isset($_POST["supprimerProjectionSalle"]))
			{
				$idSalle=htmlspecialchars($_POST['idSalle']);
				$idProj=htmlspecialchars($_POST['idProjection']);
				supprimerProjectionSalle($idSalle, $idProj);
			}
			if(isset($_POST["supprimerProjection"]))
			{
				$idProj=htmlspecialchars($_POST['idProjection']);
				supprimerProjection($idProj);
			}

			break;

		case 'SUPPRIMER':
			
			
			break;
			
		case 'MODIFIER':
			// Modifier un produit - on récupère l'id
			
			break;
		default:
			break;
	}
	
	function recupererUneProjection($creneau, $idF)
	{
		global $conn;
		$query = "SELECT * FROM Projection WHERE creneauprojection='".$creneau."' AND idFilm ='".$idF."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	// Récupérer la liste des projections d'un film
	function recupererListeProjectionsFilm($idFilm)
	{
		global $conn;
		$query = "SELECT * FROM Projection WHERE idFilm='".$idFilm."'";
		
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	
	// Récupérer la salle d'une projection
	function recupererSalleProjection($idP)
	{
		global $conn;
		$query = "SELECT nomSalle FROM Salle, projectionSalles WHERE idProjection='".$idP."' AND projectionSalles.idSalle = Salle.idSalle";
		
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    
    function addProjection($dateProjection, $creneauProjection, $idFilm) {
		global $conn;
        $query = "INSERT INTO Projection(dateProjection,creneauProjection,idFilm) values('".$dateProjection."','".$creneauProjection."','".$idFilm."')";
        mysqli_query($conn, $query);
	}

	function addProjectionSalle($idSalle, $idProj)
	{
		global $conn;
        $query = "INSERT INTO projectionSalles(idSalle,idProjection) values('".$idSalle."','".$idProj."')";
        mysqli_query($conn, $query);
	}



	 function modifierProjection($dateProjection, $creneauProjection, $idFilm,$idProjection) {
		global $conn;
        $query = "UPDATE Projection SET dateProjection =  '".$dateProjection."', creneauProjection = '".$creneauProjection."' WHERE idFilm = '".$idFilm."' AND idProjection = '".$idProjection."' ";
        mysqli_query($conn, $query);
	}

	 function modifierProjectionSalle($idSalle, $idProj) {
		global $conn;
        $query = "UPDATE Projection SET idSalle = '".$idSalle."' WHERE idProjection = '".$idProj."'";
        mysqli_query($conn, $query);
	}



	
	function supprimerProjectionSalle($idSalle, $idProj){
		global $conn;
		$query = "DELETE FROM projectionSalles WHERE idSalle = '".$idSalle."' AND idProjection = '".$idProj."'";
		mysqli_query($conn, $query);
		echo "Projection de la salle supprimée";
	}

	function supprimerProjection($idProj)
	{
		global $conn;
		$query = "DELETE FROM Projection WHERE idProjection = '".$idProj."'";
		mysqli_query($conn, $query);
		echo "Projection supprimée";
	}
	

	
?>