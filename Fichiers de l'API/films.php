<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, SUPPRIMER, MODIFIER, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


	include("db_connect.php");
	$methode = $_SERVER["REQUEST_METHOD"];
	
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
		case 'GET':
			
			if (isset($_GET["idCategorie"])) {
				$idCateg = htmlspecialchars($_GET["idCategorie"]);
				getFilmCateg($idCateg);
			}
			if (isset($_GET["nomFilm"])){
				$nom = htmlspecialchars($_GET["nomFilm"]);
				getFilmParSonNom($nom);				
			}
			else
			{
				// On récupère tous les produits
				getFilm();
			}
			
			break;
			
		case 'POST':
			break;

		case 'SUPPRIMER':
			
			
			break;
			
		case 'MODIFIER':
			// Modifier un produit - on récupère l'id
			
			break;
		default:
			break;
	}

	function getFilm()
	{
		global $conn;
		$query = "SELECT * FROM Film";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	function getFilmCateg($idCateg)
	{
		global $conn;
		$query = "SELECT * FROM Film,appartenirCategorie WHERE Film.idFilm = appartenirCategorie.idFilm AND appartenirCategorie.idCategorie ='".$idCateg."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	
	// Récupérer les informations d'un film par son nom

	function getFilmParSonNom($nomDuFilm)
	{
		global $conn;
		$query = "SELECT * FROM Film WHERE nomFilm='".$nomDuFilm."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response);
	}

	
?>