<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, SUPPRIMER, MODIFIER, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


	include("db_connect.php");
	$methode = $_SERVER["REQUEST_METHOD"];
	
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
		case 'GET':
			// On récupère tous les produits
			getProducts();
			break;
			
		case 'POST':
			// Ajouter un produit
			addProduct();
			break;

		case 'SUPPRIMER':
			// Supprimer un produit - on récupère l'id
			if(isset($_GET["id"])) {
				$id = $_GET["id"];
				deleteProductWithId($id);
			}
			break;
			
		case 'MODIFIER':
			// Modifier un produit - on récupère l'id
			
			break;
		default:
			break;
	}

	function getProducts()
	{
		global $conn;
		$query = "SELECT * FROM produits ORDER BY nomProduit";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	function addProduct()
	{
		global $conn;
		if(isset($_POST["name"])) {
			$name = $_POST["name"];
		} else {
			$name = "Pas défini";
		}
		if(isset($_POST["num"])) {
			$num = $_POST["num"];
		} else {
			$num = "Nope";
		}

		$query="INSERT INTO produits(nomProduit, numProduit) VALUES('".$name."', '".$num."')";
		mysqli_query($conn, $query);
		header('Location: http://localhost:8888/');
		echo json_encode($response);
	}

	function addProductWithID($id)
	{
		global $conn;
		$query="INSERT INTO produits(nomProduit, numProduit) VALUES('".$id."', '".$id."')";
		mysqli_query($conn, $query);
		header('Location: http://localhost:8888/');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}

	function deleteProductWithId($id)
	{
		global $conn;
		$query="DELETE FROM `produits` WHERE numProduit=".$id;
		mysqli_query($conn, $query);
		header('Location: http://localhost:8888/');
		echo json_encode($response);
	}
	
?>