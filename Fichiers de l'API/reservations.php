<?php
	// Important : Configuration de l'accès à l'API
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, SUPPRIMER, MODIFIER, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

	include("db_connect.php");

	$methode = $_SERVER["REQUEST_METHOD"];
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
        case 'GET':
            if(isset($_GET['listeInvites']))
            {
                getListeInvites();
            }
            else if(isset($_GET['idVIP']))
            {
                $id = htmlspecialchars($_GET['idVIP']);
                getInviteWithID($id);
            }
            else if(isset($_GET['idReserInv']))
            {
                $id = htmlspecialchars($_GET['idReserInv']);
                getReservation($id);
            }
            else if(isset($_GET['idVIPReser']))
            {
                $id = htmlspecialchars($_GET['idVIPReser']);
                getReservationWithInvite($id);
            }
            
			break;
		case 'POST':
			if(isset($_POST['equipeID']))
            {
                $donnees=array($_POST["idHebergement"],$_POST["idVIP"],$_POST["equipeID"],NULL);
				addReservation($donnees);
			} 
            elseif(isset($_POST['juryID']))
            {
                $donnees=array($_POST["idHebergement"],$_POST["idVIP"],NULL,$_POST["juryID"]);
				addReservation($donnees);
			} 
            elseif(isset($_POST['idSupp']))
            {
                $id = htmlspecialchars($_POST['idSupp']);
                suppReservation($id);
            }
            else
            {
                $donnees=array($_POST["idHebergement"],$_POST["idVIP"],NULL,NULL);
				addReservation($donnees);
            }
			break;
            
		default:
			break;
	}

    function getListeInvites()
    {
        global $conn;
		$query = "SELECT * FROM VIP";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
    }

    function getInviteWithID($id)
    {
        global $conn;
		$query = "SELECT * FROM VIP where idVIP='".$id."'";
        $response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    function getReservation($id)
    {
        global $conn;
		$query = "SELECT * FROM `ReservationInvite` where idReservation='".$id."'";
        $response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
    }

    function getReservationWithInvite($id)
    {
        global $conn;
		$query = "SELECT * FROM `ReservationInvite` where idVIP='".$id."'";
        $response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}
	
	function addReservation($donnees)
    {
        global $conn;
        $query = "INSERT INTO Reservation(idHebergement,idVIP,equipeID,juryID) values('".
            $donnees[0]."','".
            $donnees[1]."','".
            $donnees[2]."','".
            $donnees[3]."')";
		mysqli_query($conn, $query);
		echo "ajouté";
    }

    function suppReservation($id)
    {
        global $conn;
        $query = "DELETE FROM Reservation where idReservation='".$id."'";
        mysqli_query($conn, $query);
        echo "supprimé";
    }









?>