<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, SUPPRIMER, MODIFIER, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


	include("db_connect.php");
	$methode = $_SERVER["REQUEST_METHOD"];
	
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
		case 'GET':
			if(isset($_GET['nomCategorie']))
            {
                $nom=htmlspecialchars($_GET['nomCategorie']);
				getCategorieParNom($nom);
			}
			if(isset($_GET['nomDuFilm']))
            {
                $nom=htmlspecialchars($_GET['nomDuFilm']);
				getAllCategoriesFilm($nom);
            }
			else {
				getAllCategories();
			}
			
			break;
			
		case 'POST':
			if(isset($_POST["modifNbMax"]))
			{
				$nomCat=htmlspecialchars($_POST['nomCategorie']);
				$nbMaxJury=htmlspecialchars($_POST['nbMaxPourJury']);
				updateNbMaxVisualisations($nomCat, $nbMaxJury);
			}
			break;

		case 'SUPPRIMER':
			
			
			break;
			
		case 'MODIFIER':
						
			break;
		default:
			break;
	}

    // Récupérer toutes les catégories 
    
	function getAllCategories()
	{
		global $conn;
		$query = "SELECT * FROM CategorieFilm";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	// Récupérer une catégorie en fonction de son nom

	function getCategorieParNom($nom) {
		global $conn;
		$query = "SELECT * FROM CategorieFilm WHERE nomCategorie='".$nom."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response);
	}

	// Récupérer la liste des catégories d'un film

	function getAllCategoriesFilm($nomFilm)
	{
		global $conn;
		$query = "SELECT CategorieFilm.idCategorie, nomCategorie, nbJourProject FROM CategorieFilm,appartenirCategorie, Film WHERE Film.nomFilm='".$nomFilm."' 
		AND Film.idFilm = appartenirCategorie.idFilm AND appartenirCategorie.idCategorie = CategorieFilm.idCategorie";
		
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	function updateNbMaxVisualisations($nomCat, $nbMaxJury)
	{
		global $conn;
		$query = "UPDATE CategorieFilm SET nbMaxPourJury = '".$nbMaxJury."' WHERE nomCategorie = '".$nomCat."'";
		mysqli_query($conn, $query);
		echo "Catégorie modifiée !";
	}



	
?>