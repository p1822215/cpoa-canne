<?php
	// Important : Configuration de l'accès à l'API
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, SUPPRIMER, MODIFIER, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

	include("db_connect.php");

	$methode = $_SERVER["REQUEST_METHOD"];
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
		case 'GET':
			// On récupère les hébergements
			if(isset($_GET["idUtilisateur"]))
            {
                $idUtilisateur=htmlspecialchars($_GET['idUtilisateur']);
				getAllHebergementOfGerant($idUtilisateur);
			} 
            else if (isset($_GET["idHebergement"])) {
				$idHebergement=$_GET['idHebergement'];
				getHebergementWithId($idHebergement);
			}
            else if(isset($_GET['nomHeber']))
            {
                $nom=htmlspecialchars($_GET['nomHeber']);
				getHebergementWithName($nom);
			}
			else if(isset($_GET['tousAvecPlace']))
            {
				getTousHebergementsAvecPlace();
			}
			else if(isset($_GET['type']))
            {
				$type = $_GET['type'];
				$idGroupe = $_GET['idGroupe'];
				getTousHebergementsAvecPlaceOfGRoup($idGroupe, $type);
			}
			else if(isset($_GET['jury']))
            {
				getTousHebergementsSansReservationJuRyEquipe();
			}
			else if(isset($_GET['equipe']))
            {
				getTousHebergementsSansReservationJury();
			}
			else {
				getAllHebergements();
			}
			break;
            
		case 'POST':
			if(isset($_POST["update"])) {
				$donnees=array($_POST["nom"],$_POST["nbPlaces"],$_POST["adresse"],$_POST["ville"],$_POST["cp"],$_POST["type"],$_POST["commentaire"],$_POST["imageHeber"],$_POST["idHeber"]);
				modifHebergement($donnees);
				return;
			}
		   if(isset($_POST['nom'])) {
				$donnees=array($_POST["nom"],$_POST["nbPlaces"],$_POST["adresse"],$_POST["ville"],$_POST["cp"],$_POST["type"],$_POST["commentaire"],$_POST["imageHeber"],$_POST["idUtilisateur"]);
            	addHebergement($donnees,$nomTab);
		   }
		   // Supprimer un hébergement - on récupère l'id
			if(isset($_POST["idToDelete"])) {
				$id = $_POST["idToDelete"];
				supprimerHebergementWithID($id);
			}
		   break;
		default:
			break;
	}

	/*
		Fonction qui récupère tous les hébergements de la base de données
	*/
	function getAllHebergements() {
		global $conn;
		$query = "SELECT * FROM ListeHebergements";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*
		Fonction qui récupère les hébergements de la base de données correspondant à un gérant
		Paramètres : 
			- $idGerant : id du gérant dont on veut récupérer les hébergements
	*/
	function getAllHebergementOfGerant($idUtilisateur) {
		global $conn;
		$query = "SELECT * FROM `ListeHebergements` WHERE idUtilisateur = '".$idUtilisateur."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*
		Fonction qui récupère l'hébergement de la base de données correspondant à un id
		Paramètres : 
			- $idHebergement : id de l'hébergement que l'on veut
	*/
	function getHebergementWithId($idHebergement) {
		global $conn;
		$query = "SELECT * FROM `HebergementsWithPlaceRestante` WHERE idHebergement = '".$idHebergement."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*
		Fonction qui récupère l'hébergement de la base de données correspondant à un nom
		Paramètres : 
			- $nom : nom de l'hébergement que l'on veut
	*/
    function getHebergementWithName($nom) {
		global $conn;
		$query = "SELECT idHebergement FROM `Hebergement` WHERE nomHebergement='".$nom."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response);
	}

	/*
		Fonction qui ajoute un hébergement de la base de données
		Paramètres : 
			- $donness : les données de l'hébergement à ajouter
	*/
    function addHebergement($donnees)
    {
        global $conn;
        $query = "INSERT INTO Hebergement(nomHebergement,nbPlacesTotal,adresse,ville,cp,type,infoComplementaire,imageHeber,idUtilisateur) values('".
            $donnees[0]."','".
            $donnees[1]."','".
            $donnees[2]."','".
            $donnees[3]."','".
            $donnees[4]."','".
            $donnees[5]."','".
            $donnees[6]."','".
            $donnees[7]."','".
            $donnees[8]."')";
        mysqli_query($conn, $query);
    }

	/*
		Fonction qui modifie un hébergement de la base de données
		Paramètres : 
			- $donnees : données de l'hébergement que l'on veut modifier
	*/
    function modifHebergement($donnees)
    {
		global $conn;
        $query = "update Hebergement set nomHebergement='".$donnees[0]."', nbPlacesTotal=".$donnees[1].", adresse='".$donnees[2]."', ville='".$donnees[3]."', cp=".$donnees[4].", type=".$donnees[5].", infoComplementaire='".$donnees[6]."', imageHeber='".$donnees[7]."'  where idHebergement=".$donnees[8]."";
		mysqli_query($conn, $query);
		echo "update Hebergement set nomHebergement='".$donnees[0]."', nbPlacesTotal=".$donnees[1].", adresse='".$donnees[2]."', ville='".$donnees[3]."', cp=".$donnees[4].", type=".$donnees[5].", infoComplementaire='".$donnees[6]."', imageHeber='".$donnees[7]."'  where idHebergement=".$donnees[8]."";
	}
	
	/*
		Fonction qui supprime un hébergement de la base de données correspondant à un id
		Paramètres : 
			- $idHebergement : id de l'hébergement que l'on veut supprimer
	*/
	function supprimerHebergementWithID($id) {
		global $conn;
		$query = "DELETE FROM Hebergement WHERE idHebergement = '".$id."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo "Hébergement supprimé";
	}

	/*
		Fonction qui retourne tous les hébergements de la base de données avec de la place
		Paramètres : 
			aucun
	*/
	function getTousHebergementsAvecPlace() {
		global $conn;
		$query = "SELECT * FROM `HebergementsWithPlaceRestante` as h WHERE nbPlaceRestantes>0";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*
		Fonction qui retourne tous les hébergements de la base de données avec de la place et
		qui possède au moins une réservation pour un groupe (jury ou equipe)
		Paramètres : 
			- $idGroupe : id du jury ou de l'équipe
			- $type : equipeID ou juryID
	*/
	function getTousHebergementsAvecPlaceOfGRoup($idGroupe, $type) {
		global $conn;
		$query = "SELECT * FROM `HebergementsWithPlaceRestante` as h WHERE `nbPlaceRestantes` > 0 AND ".$idGroupe." IN (SELECT ".$type." from `Reservation` as r where r.idHebergement = h.idHebergement)";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*
		Fonction qui retourne tous les hébergements de la base de données avec de la place et
		qui ne possède aucune réservation concernant une équipe ou un jury
		Paramètres : 
			aucun
	*/
	function getTousHebergementsSansReservationJuRyEquipe() {
		global $conn;
		$query = "SELECT * FROM `HebergementsWithPlaceRestante` as lh WHERE lh.nbPlaceRestantes > 0 AND lh.idHebergement NOT IN (SELECT r.idHebergement from `Reservation` as r where r.idHebergement = lh.idHebergement and r.equipeID != 0 or r.juryID != 0);";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*
		Fonction qui retourne tous les hébergements de la base de données avec de la place et
		qui ne possède aucune réservation concernant un jury
		Paramètres : 
			aucun
	*/
	function getTousHebergementsSansReservationJury() {
		global $conn;
		$query = "SELECT * FROM `HebergementsWithPlaceRestante` AS lh WHERE lh.nbPlaceRestantes > 0 and lh.idHebergement NOT IN (select r.idHebergement from `Reservation` as r where r.idHebergement = lh.idHebergement and r.juryID != 0)";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}


?>