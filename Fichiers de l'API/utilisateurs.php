<?php
	// Important : Configuration de l'accès à l'API
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, SUPPRIMER, MODIFIER, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

	include("db_connect.php");

	$methode = $_SERVER["REQUEST_METHOD"];
	// On regarde quelle méthode est utilisée
	switch($methode)
	{
		case 'GET':
			// On récupère les utilisateurs
			if(isset($_GET["idUtilisateur"]))
            {
                $idUtilisateur=htmlspecialchars($_GET['idUtilisateur']);
				getUtilisateurWithId($idUtilisateur);
            } else {
				getAllUtilisateurs();
			}
			break;
		case 'POST':
			// Ajouter un utilisateur

			break;
		case 'SUPPRIMER':
			// Supprimer un utilisateur - on récupère l'id
			if(isset($_GET["id"])) {
				$id = $_GET["id"];
			
			}
			break;
		case 'MODIFIER':
			// Modifier un utilisateur
			
			break;
		default:
			break;
	}

	/*
		Fonction qui récupère tous les utilisateurs de la base de données
	*/
	function getAllUtilisateurs() {
		global $conn;
		$query = "SELECT * FROM Utilisateur";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*
		Fonction qui récupère l'utilisateur correspondant à un id
		Paramètres : 
			- $idGerant : id du gérant dont on veut récupérer les hébergements
	*/
	function getUtilisateurWithId($idUtilisateur) {
		global $conn;
		$query = "SELECT * FROM `Utilisateur` WHERE idUtilisateur = '".$idUtilisateur."'";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		echo json_encode($response, JSON_PRETTY_PRINT);
	}


?>