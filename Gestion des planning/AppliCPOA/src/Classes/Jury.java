/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author Nash
 */
public class Jury {
    private int idJury;
    private String nomJury;
    private int nbMaxFilms;
    
    
    public Jury()
    {
        idJury = -1;
        nomJury = null;
        nbMaxFilms = -1;
    }
    
    public Jury(int id, String nom, int nbFilms)
    {
        idJury = id;
        nomJury = nom;
        nbMaxFilms = nbFilms;   
    }
    
    /**
     * @return the idJury
     */
    public int getIdJury() {
        return idJury;
    }

    /**
     * @return the nomJury
     */
    public String getNomJury() {
        return nomJury;
    }

    /**
     * @return the nbMaxFilms
     */
    public int getNbMaxFilms() {
        return nbMaxFilms;
    }

    /**
     * @param idJury the idJury to set
     */
    public void setIdJury(int idJury) {
        this.idJury = idJury;
    }

    /**
     * @param nomJury the nomJury to set
     */
    public void setNomJury(String nomJury) {
        this.nomJury = nomJury;
    }

    /**
     * @param nbMaxFilms the nbMaxFilms to set
     */
    public void setNbMaxFilms(int nbMaxFilms) {
        this.nbMaxFilms = nbMaxFilms;
    }


}


