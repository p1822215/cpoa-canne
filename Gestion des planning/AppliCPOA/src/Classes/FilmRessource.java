/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import static Classes.ConnexionMain.client;
import static Classes.ConnexionMain.request;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Nash
 */
public class FilmRessource {

   
    public FilmRessource()
    {
        
    }
    
    public static ArrayList<Film> recupererListeFilms()
    {   
        // ArrayList qui va contenir tous les objets Categorie
        ArrayList<Film> listeFilms = new ArrayList<Film>();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/films.php")).build();
        
        listeFilms = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(FilmRessource::parseListeFilms)
                //.thenAccept(System.out::println)
                .join();
                

        return listeFilms;
  
    }
    
    public static Film recupererUnFilmParSonNom(String nomFilm)
    {
        Film objetFilm = new Film();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/films.php?nomFilm="+nomFilm)).build();
        
        objetFilm = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(FilmRessource::parseUnFilm) 
                //.thenAccept(System.out::println)
                .join();

        return objetFilm;
    }
   
    
    public static void recupererListeFilmsParCateg(int idC)
    {   
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/films.php?idCategorie="+ idC)).build();
           
        // Le client http envoie la requête 
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenAccept(System.out::println)
                .join();
    }
    
    
    
    
    // PARSE
    
    public static ArrayList<Film> parseListeFilms(String responseBody)
    {    
        JSONArray films = new JSONArray(responseBody);
        
        // ArrayList qui va contenir tous les objets Categorie
        ArrayList<Film> listeFilms = new ArrayList<Film>();

        for(int i = 0; i<films.length(); i++)
        {
            
                JSONObject film = films.getJSONObject(i);
                int id = film.getInt("idFilm");
                String nomFilm = film.getString("nomFilm");
                
                // On récupère la durée en String et on le convertit en Date sous le format hh:mm:ss
                String dureeTxt = film.getString("dureeFilm");
                
                Film objetFilm = new Film(id, nomFilm, dureeTxt);
                listeFilms.add(objetFilm);
        }

        return listeFilms;
    }
    
    public static Film parseUnFilm(String responseBody)
    {
        try {
            JSONArray films = new JSONArray(responseBody);
            
            JSONObject unFilm = films.getJSONObject(0); // Le tableau de données ne contient qu'un objet donc index 0
            int id = unFilm.getInt("idFilm");
            String nomFilm = unFilm.getString("nomFilm");
            
            // On récupère la durée en String et on le convertit en Date sous le format hh:mm:ss
            String dureeTxt = unFilm.getString("dureeFilm");
            
            // On récupération la liste des catégories du film
            ArrayList<CategorieFilm> listeCategories = new ArrayList();
            CategorieRessource categorieManager = new CategorieRessource();
            
            //Encodage du nom du film pour la requête
            String nomEncodage = URLEncoder.encode(nomFilm, "UTF-8");
            listeCategories = categorieManager.recupererListeCategoriesPourUnFilm(nomEncodage);
            
            Film objFilm = new Film(id, nomFilm, dureeTxt, listeCategories);
            
            return objFilm;
            
        } 
        catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FilmRessource.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
    
    public static Film parseFilms(String responseBody)
    {

        JSONArray films = new JSONArray(responseBody);

        JSONObject film = films.getJSONObject(0);

        int id = film.getInt("idFilm");
        String nomFilm = film.getString("nomFilm");
        
        // On récupère la durée en String et on le convertit en Date sous le format hh:mm:ss
        String dureeTxt = film.getString("dureeFilm");
        DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        
        try {
            Date duree = sdf.parse(dureeTxt);
        } catch (ParseException ex) {
            System.out.println("Erreur parse");
            Logger.getLogger(FilmRessource.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return null;
    }
    
    

    // FONCTION GESTIONDONNEES
            
    private static void gestionDonnees(Map<String, Object> tableauParametres, URL url) throws UnsupportedEncodingException, ProtocolException, IOException
    {
        // On encode les données des paramètres
        StringBuilder postData = new StringBuilder();
        for(Map.Entry<String, Object> uneDonnee : tableauParametres.entrySet())
        {
            if (postData.length() != 0) {
                postData.append('&');
            }
            
            postData.append(URLEncoder.encode(uneDonnee.getKey(), "UTF-8"));
            postData.append("=");
            postData.append(URLEncoder.encode(String.valueOf(uneDonnee.getValue()), "UTF-8"));
        }
        
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        
        // Ouvre une connection
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        
        // Set the request method to POST
        con.setRequestMethod("POST");
        
        // ajout du header de la requête <head></head>
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        
        //Ensure the Connection Will Be Used to Send Content. Otherwise, we'll not be able to write content to the connection output stream
        con.setDoOutput(true);
        con.getOutputStream().write(postDataBytes);
        
        Reader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
        
        for (int c; (c = in.read()) >= 0;)
            System.out.print((char)c);
    }
   
 
}

