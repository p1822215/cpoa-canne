/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author Nash
 */
public class CategorieFilm {
    
    private int idCategorie;
    private String nomCategorie;
    private int nbJourProject;
    private int nbDiffusionParJour;
    private int nbMaxVisualiserJury;

    public CategorieFilm()
    {
        idCategorie = -1;
        nomCategorie = null;
        nbJourProject = -1;
        nbMaxVisualiserJury = -1;
    }
    
    public CategorieFilm(int id, String nom, int nbJ)
    {
        idCategorie = id;
        nomCategorie = nom;
        nbJourProject = nbJ;
    }
    
    public CategorieFilm(int id, String nom, int nbJ,int nbDiffusion, int nbMax)
    {
        idCategorie = id;
        nomCategorie = nom;
        nbJourProject = nbJ;
        nbDiffusionParJour = nbDiffusion;
        nbMaxVisualiserJury = nbMax;
    }
    
    /**
     * @return the idCategorie
     */
    public int getIdCategorie() {
        return idCategorie;
    }

    /**
     * @param idCategorie the idCategorie to set
     */
    public void setIdCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }

    /**
     * @return the nomCategorie
     */
    public String getNomCategorie() {
        return nomCategorie;
    }

    /**
     * @param nomCategorie the nomCategorie to set
     */
    public void setNomCategorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
    }

    /**
     * @return the nbJourProject
     */
    public int getNbJourProject() {
        return nbJourProject;
    }

    /**
     * @param nbJourProject the nbJourProject to set
     */
    public void setNbJourProject(int nbJourProject) {
        this.nbJourProject = nbJourProject;
    }

    /**
     * @return the nbMaxVisualiserJury
     */
    public int getNbMaxVisualiserJury() {
        return nbMaxVisualiserJury;
    }

    /**
     * @param nbMaxVisualiserJury the nbMaxVisualiserJury to set
     */
    public void setNbMaxVisualiserJury(int nbMaxVisualiserJury) {
        this.nbMaxVisualiserJury = nbMaxVisualiserJury;
    }

    /**
     * @return the nbDiffusionParJour
     */
    public int getNbDiffusionParJour() {
        return nbDiffusionParJour;
    }

    /**
     * @param nbDiffusionParJour the nbDiffusionParJour to set
     */
    public void setNbDiffusionParJour(int nbDiffusionParJour) {
        this.nbDiffusionParJour = nbDiffusionParJour;
    }
  
    
}
