/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import static Classes.ConnexionMain.client;
import static Classes.ConnexionMain.request;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Nash
 */
public class ProjectionRessource {
    
    public ProjectionRessource()
    {
        
    }
    
    // RECUPERER UNE PROJECTION PAR LE CRENEAU ET PAR L'ID DU FILM (REQUETE GET)
    
     public static Projection recupererUneProjection(String creneau, int idF)
    {
        // ArrayList qui va contenir tous les objets Categorie
        Projection objProjection = new Projection();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/projection.php?creneauProjection="+creneau+"&idFilm="+idF)).build();
        
        objProjection = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(ProjectionRessource::parseUneProjection) 
                //.thenAccept(System.out::println)
                .join();

        return objProjection;
    }
     
    // RECUPERER LISTE DE PROJECTIONS D'UN FILM
     
    public static ArrayList<Projection> recupererListeProjectionsPourUnFilm(int idFilm)
    {   
        // ArrayList qui va contenir tous les objets Projection
        ArrayList<Projection> listeProjections = new ArrayList<Projection>();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/projection.php?idDuFilm="+idFilm)).build();
        
        listeProjections = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(ProjectionRessource::parseProjections) 
                //.thenAccept(System.out::println)
                .join();

        return listeProjections;
  
    }
    
    // RECUPERER SALLE D'UNE PROJECTION
    public static String recupererSalleProjection(int idProj)
    {
        String nomSalle = null;
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/projection.php?idProjection="+idProj)).build();
        
        nomSalle = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(ProjectionRessource::parseSalleProjection) 
                //.thenAccept(System.out::println)
                .join();
        
        
        return nomSalle;
    }
            
    
    // AJOUTER UNE PROJECTION DANS LA TABLE PROJECTION (REQUETE POST)
     
    public static void ajoutProjection(String dateP, String creneau, int idF) throws Exception {
        
        //Créer objet URL
        URL url = new URL ("http://projetcannes.alwaysdata.net/projection.php");
        
        // Paramètres de la requête
        Map<String, Object> donneesProjection = new LinkedHashMap<>();
        donneesProjection.put("ajoutProjection", "");
        donneesProjection.put("dateProjection", dateP);
        donneesProjection.put("creneauProjection", creneau);
        donneesProjection.put("idFilm", idF); 
        
        gestionDonnees(donneesProjection, url);
       

    }

    
    // AJOUTER UNE PROJECTION DANS LA TABLE PROJECTIONSALLES (REQUETE POST)
    
    public static void ajoutProjectionDansUneSalle(int idSalle, int idProj) throws Exception 
    {
        //Créer objet URL
        URL url = new URL ("http://projetcannes.alwaysdata.net/projection.php");
        
        // Paramètres de la requête
        Map<String, Object> donneesProjection = new LinkedHashMap<>();
        donneesProjection.put("ajoutProjectionSalle", "");
        donneesProjection.put("idSalle", idSalle);
        donneesProjection.put("idProjection", idProj);
              
        gestionDonnees(donneesProjection, url);
    }
    
    
    public static void modifierProjection(String dateP, String creneau, int idF, int idP) throws Exception {
        
        //Créer objet URL
        URL url = new URL ("http://projetcannes.alwaysdata.net/projection.php");
        
        // Paramètres de la requête
        Map<String, Object> donneesProjection = new LinkedHashMap<>();
        donneesProjection.put("modifierProjection", "");
        donneesProjection.put("dateProjection", dateP);
        donneesProjection.put("creneauProjection", creneau);
        donneesProjection.put("idFilm", idF); 
        donneesProjection.put("idProjection", idP);
        
        gestionDonnees(donneesProjection, url);
    }
    
    public static void modifierProjectionDansUneSalle(int idSalle, int idProj) throws Exception 
    {
        //Créer objet URL
        URL url = new URL ("http://projetcannes.alwaysdata.net/projection.php");
        
        // Paramètres de la requête
        Map<String, Object> donneesProjection = new LinkedHashMap<>();
        donneesProjection.put("modifierProjectionSalle", "");
        donneesProjection.put("idSalle", idSalle);
        donneesProjection.put("idProjection", idProj);
              
        gestionDonnees(donneesProjection, url);
    }
    
    
    // SUPPRIMER UNE PROJECTION D'UNE SALLE
    public static void supprimerProjectionSalle(int idS, int idP) throws Exception
    {
        //Créer objet URL
        URL url = new URL ("http://projetcannes.alwaysdata.net/projection.php");
        
        // Paramètres de la requête
        Map<String, Object> donneesProjection = new LinkedHashMap<>();
        donneesProjection.put("supprimerProjectionSalle", "");
        donneesProjection.put("idSalle", idS);
        donneesProjection.put("idProjection", idP);
        
        gestionDonnees(donneesProjection, url);
         
    }
    
    // SUPPRIMER UNE PROJECTION DE LA TABLE PROJECTION
    public static void supprimerProjection(int idP) throws Exception
    {
        //Créer objet URL
        URL url = new URL ("http://projetcannes.alwaysdata.net/projection.php");
        
        // Paramètres de la requête
        Map<String, Object> donneesProjection = new LinkedHashMap<>();
        donneesProjection.put("supprimerProjection", "");
        donneesProjection.put("idProjection", idP);
        
        gestionDonnees(donneesProjection, url);
    }
    
    
    // PARSE LES DONNEES RECUPERER AVEC LES REQUETES GET
    
    
    public static Projection parseUneProjection(String responseBody)
    {
        JSONArray projections = new JSONArray(responseBody);

        JSONObject uneProjection = projections.getJSONObject(0); // Le tableau de données ne contient qu'un objet donc index 0
        int id = uneProjection.getInt("idProjection");
        String nomSalle = uneProjection.getString("dateProjection");
        String creneau = uneProjection.getString("creneauProjection");
        
        Projection objProj = new Projection(id, nomSalle, creneau);

        return objProj;
    }
    
    
    public static ArrayList<Projection> parseProjections(String responseBody)
    {
        JSONArray projections = new JSONArray(responseBody);
        
        // ArrayList qui va contenir tous les objets Projection
        ArrayList<Projection> listeProjections = new ArrayList<Projection>();

        for(int i = 0; i<projections.length(); i++)
        {
            JSONObject uneProjection = projections.getJSONObject(i);
            int id = uneProjection.getInt("idProjection");
            String dateP = uneProjection.getString("dateProjection");
            String creneau = uneProjection.getString("creneauProjection");
            
            Projection objProjection = new Projection(id, dateP, creneau);
            listeProjections.add(objProjection);
            
        }

        return listeProjections;

    }
    
    public static String parseSalleProjection(String responseBody)
    {
        JSONArray salles = new JSONArray(responseBody);
        
        String nomSalle = null;

        JSONObject uneSalle = salles.getJSONObject(0); // Le tableau de données ne contient qu'un objet donc index 0
        nomSalle = uneSalle.getString("nomSalle");

        return nomSalle;
    }
    
    
    
    // FONCTION GESTIONDONNEES
            
    private static void gestionDonnees(Map<String, Object> tableauParametres, URL url) throws UnsupportedEncodingException, ProtocolException, IOException
    {
        // On encode les données des paramètres
        StringBuilder postData = new StringBuilder();
        for(Map.Entry<String, Object> uneDonnee : tableauParametres.entrySet())
        {
            if (postData.length() != 0) {
                postData.append('&');
            }
            
            postData.append(URLEncoder.encode(uneDonnee.getKey(), "UTF-8"));
            postData.append("=");
            postData.append(URLEncoder.encode(String.valueOf(uneDonnee.getValue()), "UTF-8"));
        }
        
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        
        // Ouvre une connection
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        
        // Set the request method to POST
        con.setRequestMethod("POST");
        
        // ajout du header de la requête <head></head>
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        
        //Ensure the Connection Will Be Used to Send Content. Otherwise, we'll not be able to write content to the connection output stream
        con.setDoOutput(true);
        con.getOutputStream().write(postDataBytes);
        
        Reader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
        
        for (int c; (c = in.read()) >= 0;)
            System.out.print((char)c);
    }

}
