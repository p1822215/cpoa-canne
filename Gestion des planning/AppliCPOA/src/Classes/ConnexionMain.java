/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.ArrayList;

/**
 *
 * @author Nash
 */
public class ConnexionMain {
        
    public static HttpClient client = HttpClient.newHttpClient();
    public static HttpRequest request;
    
    public static void main(String[] args) throws UnsupportedEncodingException, Exception {
        
       // Récupère un film
       
       /*FilmRessource f1 = new FilmRessource(); 
       Film filmm = f1.recupererUnFilm();
       
       String nom = filmm.getNomFilm();
       System.out.println(nom);*/
       
       // Récupère liste des catégories
       
       /*CategorieRessource catManager = new CategorieRessource();
       
       ArrayList<CategorieFilm> listeCategories = new ArrayList<CategorieFilm>();
       listeCategories = catManager.recupererListeCategories();      
       
        System.out.println("Liste des catégories : ");
        System.out.println("");
        
       for (CategorieFilm uneCategorie : listeCategories) {
            System.out.println("Nom : " + uneCategorie.getNomCategorie());
            System.out.println("Nombre de jours de projection : " + uneCategorie.getNbJourProject());
            System.out.println("");
        }*/
       
       
       // Récupère liste des salles
       
       /*ArrayList<Salle> listeSalles = new ArrayList();
       SalleRessource SalleManager = new SalleRessource();
       listeSalles = SalleManager.recupererSalles();      
       
        System.out.println("Liste des salles : ");
        System.out.println("");
        
       for (Salle uneSalle : listeSalles) {
            System.out.println("Nom : " + uneSalle.getNomSalle());
            System.out.println("Nombre de places : " + uneSalle.getNbPlace());
            System.out.println("");
       }*/
       
       
       // RECUPERER UNE CATEGORIE PAR SON NOM
       
       /*CategorieRessource catMan = new CategorieRessource();
       String nom = "Longs Metrages";
       // Quand on passe un String avec espace dans l'url, il faut d'abord encoder la variable pour enlever les espaces !
       String nomEncodage = URLEncoder.encode(nom, "UTF-8");
       
       CategorieFilm objCat = new CategorieFilm();
       objCat = catMan.recupererUneCategorie(nomEncodage);
       
        System.out.println("Informations d'une catégorie cherchée par son nom : ");
        System.out.println("Nom : " + objCat.getNomCategorie());
        System.out.println("Nombre de jours de projection : " + objCat.getNbJourProject());
        System.out.println("Nombre max que peut visualiser un jury : " + objCat.getNbMaxVisualiserJury());*/
        
       
       /*FilmRessource filmManage = new FilmRessource();
       filmManage.recupererListeFilmsParCateg(1);*/
       
       
       // RECUPERER LISTE DES FILMS 
       /*FilmRessource filmManage = new FilmRessource();
       filmManage.recupererListeFilms();*/
       
       
       // RECUPERER LISTE CATEGORIES D'UN FILM
       /*CategorieRessource categorieManager = new CategorieRessource();
       String nom = "La petite fille aux allumettes";
       String nomEncodage = URLEncoder.encode(nom, "UTF-8");
       categorieManager.recupererListeCategoriesPourUnFilm(nomEncodage);*/
       
       // RECUPERER INFORMATIONS D'UN FILM
       /*FilmRessource filmManage = new FilmRessource();
       Film objetFilm = new Film();
       String nom = "La petite fille aux allumettes";
       String nomEncodage = URLEncoder.encode(nom, "UTF-8");
       objetFilm = filmManage.recupererUnFilmParSonNom("Joker");
       
        System.out.println("Nom du film : "+objetFilm.getNomFilm());
        long temps = java.util.concurrent.TimeUnit.MILLISECONDS.toHours(objetFilm.getDuree().getTime());
        System.out.println("Durée du film : "+temps);
        
        System.out.println("Liste des catégories du film :");
        
        ArrayList<CategorieFilm> listeCategories = new ArrayList();
        listeCategories = objetFilm.getListeCategories();
        
        for(CategorieFilm uneCategorie : listeCategories)
        {
            System.out.println(uneCategorie.getNomCategorie());
        }*/
       
       
       // RECUPERER UNE SALLE PAR SON NOM
       /*SalleRessource salleManage = new SalleRessource();
       Salle objetSalle = new Salle();
       String nom = "Le Grand Theatre Lumière";
       String nomEncodage = URLEncoder.encode(nom, "UTF-8");
       objetSalle = salleManage.recupererSalleParSonNom(nomEncodage);
       
        System.out.println("Id de la salle : "+objetSalle.getIdSalle());
        System.out.println("Nom de la salle : "+objetSalle.getNomSalle());
        System.out.println("Nombre de places : "+objetSalle.getNbPlace());*/
       
       // TEST AJOUT D'UNE PROJECTION DANS LA BDD
       
      /* ProjectionRessource projectionManager = new ProjectionRessource();
       projectionManager.ajoutProjection("12/05/2021", "Matin", 2);*/
       
       // TEST AJOUT D'UNE PROJECTION DANS UNE SALLE (projectionSalles)
       /*ProjectionRessource projectionManager = new ProjectionRessource();
       projectionManager.ajoutProjectionDansUneSalle(2,13);*/
       
       
       // RECUPERER UNE PROJECTION PAR SON CRENEAU ET ID DU FILM
       /*ProjectionRessource projectionManager = new ProjectionRessource();
       Projection p = new Projection();
       p = projectionManager.recupererUneProjection("Matin", 2);
       
       System.out.println("Id de la projection : "+p.getIdProjection());
       System.out.println("Créneau de la projection : "+p.getCrenauProjection());
       System.out.println("Date de la proj : "+ p.getDateProjection());*/
       
       // TEST SUPPRIMER UNE PROJECTION D'UNE SALLE
       /*ProjectionRessource projectionManager = new ProjectionRessource();
       projectionManager.supprimerProjectionSalle(2, 17);*/
       
       // TEST SUPPRIMER UNE PROJECTION
       /*ProjectionRessource projectionManager = new ProjectionRessource();
       projectionManager.supprimerProjection(16);*/
       
       // TEST RECUPERER LISTE PROJECTIONS D'UN FILM
       /*ProjectionRessource projectionManager = new ProjectionRessource();
       ArrayList<Projection> listeProjections = new ArrayList<Projection>();
       
       listeProjections = projectionManager.recupererListeProjectionsPourUnFilm(2);
       
       for(Projection uneProjection : listeProjections)
       {
           System.out.println("id de la projection : "+uneProjection.getIdProjection());
           System.out.println("date de la projection : "+uneProjection.getDateProjection());
           System.out.println("créneau de la projection : "+uneProjection.getCrenauProjection());
       }*/
       
       // TEST RECUPERER SALLE D'UNE PROJECTION
       /*ProjectionRessource projectionManager = new ProjectionRessource();
       String nomSalle = projectionManager.recupererSalleProjection(62);
        System.out.println("Salle : "+nomSalle);*/
       
       
       // TEST MODIFIER NOMBRE DE MAX DE VISUALISATION D'UNE CATEGORIE
       CategorieRessource catManager = new CategorieRessource();
       catManager.updateNbMaxVisualisations("Hors Competition",4);
        
    }
        
        
}
