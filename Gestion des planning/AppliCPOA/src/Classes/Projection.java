/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.Date;

public class Projection {
    private int idProjection;
    private String dateProjection;
    private String crenauProjection;
    private Salle salleProjection;
    private int idFilm;
    
    
    public Projection() {
        idProjection = -1;
        dateProjection = null;
        crenauProjection = null;
        salleProjection = null;  
    }
    
    public Projection(int id, String dateP, String horaires)
    {
        idProjection = id;
        dateProjection = dateP;
        crenauProjection = horaires;
    }
    
    public Projection(int id, String dateP, String horaires, int idF)
    {
        idProjection = id;
        dateProjection = dateP;
        crenauProjection = horaires;
        idFilm = idF;
    }
     
    public Projection(int id, String dateP, String horaires, Salle s)
    {
        idProjection = id;
        dateProjection = dateP;
        crenauProjection = horaires;
        salleProjection = s;
    }



    /**
     * @return the idProjection
     */
    public int getIdProjection() {
        return idProjection;
    }

    /**
     * @param idProjection the idProjection to set
     */
    public void setIdProjection(int idProjection) {
        this.idProjection = idProjection;
    }

    /**
     * @return the dateProjection
     */
    public String getDateProjection() {
        return dateProjection;
    }

    /**
     * @param dateProjection the dateProjection to set
     */
    public void setDateProjection(String dateProjection) {
        this.dateProjection = dateProjection;
    }

    /**
     * @return the crenauProjection
     */
    public String getCrenauProjection() {
        return crenauProjection;
    }

    /**
     * @param crenauProjection the crenauProjection to set
     */
    public void setCrenauProjection(String crenauProjection) {
        this.crenauProjection = crenauProjection;
    }

    /**
     * @return the salleProjection
     */
    public Salle getSalleProjection() {
        return salleProjection;
    }

    /**
     * @param salleProjection the salleProjection to set
     */
    public void setSalleProjection(Salle salleProjection) {
        this.salleProjection = salleProjection;
    }
    
    /**
     * @return the salleProjection
     */
    public int getIdFilm() {
        return idFilm;
    }

    /**
     * @param salleProjection the salleProjection to set
     */
    public void setIdFilm(int idFilm) {
        this.idFilm = idFilm;
    }
       
}
