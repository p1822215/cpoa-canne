 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import static Classes.ConnexionMain.client;
import static Classes.ConnexionMain.request;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Nash
 */
public class CategorieRessource {
    
    public CategorieRessource()
    {
        
    }
    
    public static ArrayList<CategorieFilm> recupererListeCategories()
    {   
        // ArrayList qui va contenir tous les objets Categorie
        ArrayList<CategorieFilm> listeCategories = new ArrayList<CategorieFilm>();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/categoriesFilm.php")).build();
        
        listeCategories = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(CategorieRessource::parseCategories) // On affiche le corps de la réponse
                //.thenAccept(System.out::println)
                .join();

        return listeCategories;
  
    }
    
    public static CategorieFilm recupererUneCategorie(String nomCat)
    {
        // ArrayList qui va contenir tous les objets Categorie
        CategorieFilm catFilm = new CategorieFilm();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/categoriesFilm.php?nomCategorie="+nomCat)).build();
        
        catFilm = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(CategorieRessource::parseUneCategorie) 
                //.thenAccept(System.out::println)
                .join();

        return catFilm;
    }
    
    public static ArrayList<CategorieFilm> recupererListeCategoriesPourUnFilm(String nomFilm)
    {   
        // ArrayList qui va contenir tous les objets Categorie
        ArrayList<CategorieFilm> listeCategories = new ArrayList<CategorieFilm>();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/categoriesFilm.php?nomDuFilm="+nomFilm)).build();
        
        listeCategories = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(CategorieRessource::parseCategories) // On affiche le corps de la réponse
                //.thenAccept(System.out::println)
                .join();

        return listeCategories;
  
    }
    
    // MODIFIER NBMAXPOURJURY D'UNE CATEGORIE (REQUETE POST)
    
    public static void updateNbMaxVisualisations(String nomCat, int nbMax) throws Exception 
    {
        //Créer objet URL
        URL url = new URL ("http://projetcannes.alwaysdata.net/categoriesFilm.php");
        
        // Paramètres de la requête
        Map<String, Object> tableauDonnees = new LinkedHashMap<>();
        tableauDonnees.put("modifNbMax", "");
        tableauDonnees.put("nomCategorie", nomCat);
        tableauDonnees.put("nbMaxPourJury", nbMax);
        
        gestionDonnees(tableauDonnees, url);
    }
    
     
    public static ArrayList<CategorieFilm> parseCategories(String responseBody)
    {
        JSONArray categories = new JSONArray(responseBody);
        
        // ArrayList qui va contenir tous les objets Categorie
        ArrayList<CategorieFilm> listeCategories = new ArrayList<CategorieFilm>();

        for(int i = 0; i<categories.length(); i++)
        {
            JSONObject uneCategorie = categories.getJSONObject(i);
            int id = uneCategorie.getInt("idCategorie");
            String nomCategorie = uneCategorie.getString("nomCategorie");
            int nbJourProjection = uneCategorie.getInt("nbJourProject");
            
            CategorieFilm objCategorie = new CategorieFilm(id, nomCategorie, nbJourProjection);
            listeCategories.add(objCategorie);
            
        }

        return listeCategories;

    }
    
      public static CategorieFilm parseUneCategorie(String responseBody)
    {
        JSONArray categories = new JSONArray(responseBody);
                
        JSONObject uneCategorie = categories.getJSONObject(0); // Le tableau de données ne contient qu'un objet donc index 0
        int id = uneCategorie.getInt("idCategorie");
        String nomCategorie = uneCategorie.getString("nomCategorie");
        int nbJourProjection = uneCategorie.getInt("nbJourProject");
        int nbDiffusion = uneCategorie.getInt("nbDiffusionParJour");
        int nbMaxJury = uneCategorie.getInt("nbMaxPourJury");

        CategorieFilm objCategorie = new CategorieFilm(id, nomCategorie, nbJourProjection,nbDiffusion, nbMaxJury);

        return objCategorie;

    }
      
      
    
    // FONCTION GESTIONDONNEES
            
    private static void gestionDonnees(Map<String, Object> tableauParametres, URL url) throws UnsupportedEncodingException, ProtocolException, IOException
    {
        // On encode les données des paramètres
        StringBuilder postData = new StringBuilder();
        for(Map.Entry<String, Object> uneDonnee : tableauParametres.entrySet())
        {
            if (postData.length() != 0) {
                postData.append('&');
            }
            
            postData.append(URLEncoder.encode(uneDonnee.getKey(), "UTF-8"));
            postData.append("=");
            postData.append(URLEncoder.encode(String.valueOf(uneDonnee.getValue()), "UTF-8"));
        }
        
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        
        // Ouvre une connection
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        
        // Set the request method to POST
        con.setRequestMethod("POST");
        
        // ajout du header de la requête <head></head>
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        
        //Ensure the Connection Will Be Used to Send Content. Otherwise, we'll not be able to write content to the connection output stream
        con.setDoOutput(true);
        con.getOutputStream().write(postDataBytes);
        
        Reader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
        
        for (int c; (c = in.read()) >= 0;)
            System.out.print((char)c);
    }
    
  
    
    
}
