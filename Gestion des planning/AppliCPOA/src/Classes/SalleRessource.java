/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import static Classes.ConnexionMain.client;
import static Classes.ConnexionMain.request;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Nash
 */
public class SalleRessource {
    
    public SalleRessource()
    {
        
    }
    
    public static ArrayList<Salle> recupererSalles()
    {
        // ArrayList qui va contenir tous les objets Salle
        ArrayList<Salle> listeSalles = new ArrayList();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/salles.php")).build();
        
        listeSalles = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(SalleRessource::parseSalles) 
                //.thenAccept(System.out::println)
                .join();

        return listeSalles;
    }
    
    public static Salle recupererSalleParSonNom(String nomSalle)
    {
        Salle objetSalle = new Salle();
        
        // On crée la requête http en indiquant l'URL correspondant
        request = HttpRequest.newBuilder().uri(URI.create("http://projetcannes.alwaysdata.net/salles.php?nomSalle="+nomSalle)).build();
        
        objetSalle = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())  //2e param : Demande au serveur le corps de la response en String
                .thenApply(HttpResponse::body) // On indique qu'on veut utiliser le corps de la réponse HTTP reçue
                .thenApply(SalleRessource::parseUneSalle) 
                //.thenAccept(System.out::println)
                .join();

        return objetSalle;
    }
    
    // Modifier nombre de place de la salle
    public static void updateNbPlaceSalle(String nomSalle, int nbP) throws Exception 
    {
        //Créer objet URL
        URL url = new URL ("http://projetcannes.alwaysdata.net/salles.php");
        
        // Paramètres de la requête
        Map<String, Object> tableauDonnees = new LinkedHashMap<>();
        tableauDonnees.put("modifNbPlace", "");
        tableauDonnees.put("nomSalle", nomSalle);
        tableauDonnees.put("nbPlaces", nbP);
        
        gestionDonnees(tableauDonnees, url);
 
    }
    
    public static ArrayList<Salle> parseSalles(String responseBody)
    {
        JSONArray salles = new JSONArray(responseBody);
        
        // ArrayList qui va contenir tous les objets Categorie
        ArrayList<Salle> listeSalles = new ArrayList();

        for(int i = 0; i<salles.length(); i++)
        {
            JSONObject uneSalle = salles.getJSONObject(i);
            int id = uneSalle.getInt("idSalle");
            String nomSalle = uneSalle.getString("nomSalle");
            int nbPlaces = uneSalle.getInt("nbPlaces");
            
           Salle salleFilm = new Salle(id, nomSalle, nbPlaces);
           listeSalles.add(salleFilm);
            
        }

        return listeSalles;

    }
    
    public static Salle parseUneSalle(String responseBody)
    {
        JSONArray salles = new JSONArray(responseBody);

        JSONObject uneSalle = salles.getJSONObject(0); // Le tableau de données ne contient qu'un objet donc index 0
        int id = uneSalle.getInt("idSalle");
        String nomSalle = uneSalle.getString("nomSalle");
        int nbP = uneSalle.getInt("nbPlaces");

        Salle objSalle = new Salle(id, nomSalle, nbP);

        return objSalle;

    }
    
    
// FONCTION GESTIONDONNEES
            
    private static void gestionDonnees(Map<String, Object> tableauParametres, URL url) throws UnsupportedEncodingException, ProtocolException, IOException
    {
        // On encode les données des paramètres
        StringBuilder postData = new StringBuilder();
        for(Map.Entry<String, Object> uneDonnee : tableauParametres.entrySet())
        {
            if (postData.length() != 0) {
                postData.append('&');
            }
            
            postData.append(URLEncoder.encode(uneDonnee.getKey(), "UTF-8"));
            postData.append("=");
            postData.append(URLEncoder.encode(String.valueOf(uneDonnee.getValue()), "UTF-8"));
        }
        
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        
        // Ouvre une connection
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        
        // Set the request method to POST
        con.setRequestMethod("POST");
        
        // ajout du header de la requête <head></head>
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        
        //Ensure the Connection Will Be Used to Send Content. Otherwise, we'll not be able to write content to the connection output stream
        con.setDoOutput(true);
        con.getOutputStream().write(postDataBytes);
        
        Reader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
        
        for (int c; (c = in.read()) >= 0;)
            System.out.print((char)c);
    }
    
}
