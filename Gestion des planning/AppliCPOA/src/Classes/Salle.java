/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;

/**
 *
 * @author Nash
 */
public class Salle {
    private int idSalle;
    private String nomSalle;
    private int nbPlace;
    private ArrayList<Projection> diffusions = new ArrayList();
    
    
    public Salle(int id, String nom, int nbP)
    {
        idSalle = id;
        nomSalle = nom;
        nbPlace = nbP;
        diffusions = null;
       
    }
    
    public Salle()
    {
        idSalle = -1;
        nomSalle = null;
        nbPlace = -1;
        diffusions = null;
       
    }
    
    public Salle(int id, String nom, int nbP, Projection p)
    {
        idSalle = id;
        nomSalle = nom;
        nbPlace = nbP;
        diffusions.add(p);
       
    }
    
    public Salle(int id, String nom, int nbP, ArrayList<Projection> listeProjections)
    {
        idSalle = id;
        nomSalle = nom;
        nbPlace = nbP;
        diffusions = listeProjections;
       
    }

    /**
     * @return the idSalle
     */
    public int getIdSalle() {
        return idSalle;
    }

    /**
     * @param idSalle the idSalle to set
     */
    public void setIdSalle(int idSalle) {
        this.idSalle = idSalle;
    }

    /**
     * @return the nomSalle
     */
    public String getNomSalle() {
        return nomSalle;
    }

    /**
     * @param nomSalle the nomSalle to set
     */
    public void setNomSalle(String nomSalle) {
        this.nomSalle = nomSalle;
    }

    /**
     * @return the nbPlace
     */
    public int getNbPlace() {
        return nbPlace;
    }

    /**
     * @param nbPlace the nbPlace to set
     */
    public void setNbPlace(int nbPlace) {
        this.nbPlace = nbPlace;
    }

    /**
     * @return the diffusions
     */
    public ArrayList<Projection> getDiffusions() {
        return diffusions;
    }

    /**
     * @param diffusions the diffusions to set
     */
    public void setDiffusions(ArrayList<Projection> diffusions) {
        this.diffusions = diffusions;
    }
           
}
