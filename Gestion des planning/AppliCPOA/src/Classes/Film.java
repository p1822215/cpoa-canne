package Classes;

import java.util.ArrayList;

public class Film {
    private int idFilm;
    private String nomFilm;
    private String duree;
    private int nbJoursProjection;
    private ArrayList<CategorieFilm> listeCategories = new ArrayList();
    private ArrayList<Projection> listeProjections = new ArrayList();
    
    
    public Film()
    {
        idFilm = -1;
        nomFilm = null;
        duree = null;
        listeCategories = null;
        listeProjections = null;
    }
        
    public Film(int id, String nom, String d)
    {
        idFilm = id;
        nomFilm = nom;
        duree = d;
        listeCategories = null;
        listeProjections = null;
    }
    
    public Film(int id, String nom, String d, CategorieFilm listeCat)
    {
        idFilm = id;
        nomFilm = nom;
        duree = d;
        listeCategories.add(listeCat);
        listeProjections = null;
    }
    
    public Film(int id, String nom, String d, ArrayList<CategorieFilm> listeCat, Projection p)
    {
        idFilm = id;
        nomFilm = nom;
        duree = d;
        listeCategories = listeCat;
        listeProjections.add(p);
    }

    
     public Film(int id, String nom, String d, ArrayList<CategorieFilm> listeCat)
    {
        idFilm = id;
        nomFilm = nom;
        duree = d;
        listeCategories = listeCat;
        listeProjections = null;
    }
    
    public Film(int id, String nom, String d, ArrayList<CategorieFilm> listeCat, ArrayList<Projection> listeP)
    {
        idFilm = id;
        nomFilm = nom;
        duree = d;
        listeCategories = listeCat;
        listeProjections = listeP;
    }

    /**
     * @return the idFilm
     */
    public int getIdFilm() {
        return idFilm;
    }

    /**
     * @param idFilm the idFilm to set
     */
    public void setIdFilm(int idFilm) {
        this.idFilm = idFilm;
    }

    /**
     * @return the nomFilm
     */
    public String getNomFilm() {
        return nomFilm;
    }

    /**
     * @param nomFilm the nomFilm to set
     */
    public void setNomFilm(String nomFilm) {
        this.nomFilm = nomFilm;
    }

    /**
     * @return the duree
     */
    public String getDuree() {
        return duree;
    }

    /**
     * @param duree the duree to set
     */
    public void setDuree(String duree) {
        this.duree = duree;
    }

    /**
     * @return the listeCategories
     */
    public ArrayList<CategorieFilm> getListeCategories() {
        return listeCategories;
    }

    /**
     * @param listeCategories the listeCategories to set
     */
    public void setListeCategories(ArrayList<CategorieFilm> listeCategories) {
        this.listeCategories = listeCategories;
    }

    /**
     * @return the listeProjections
     */
    public ArrayList<Projection> getListeProjections() {
        return listeProjections;
    }

    /**
     * @param listeProjections the listeProjections to set
     */
    public void setListeProjections(ArrayList<Projection> listeProjections) {
        this.listeProjections = listeProjections;
    }
    
    
    
    
}
