/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fenetres;

import Classes.CategorieFilm;
import Classes.CategorieRessource;
import Classes.Film;
import Classes.FilmRessource;
import Classes.Projection;
import Classes.ProjectionRessource;
import Classes.Salle;
import Classes.SalleRessource;
import static fenetres.AjoutFilms.*;
import static fenetres.ModifierFilm.creneauDest;
import static fenetres.ModifierFilm.nouveauFilm2;
import static fenetres.ModifierFilm.salleDest;
import static fenetres.ModifierFilm.suppression;
import java.util.ArrayList;
import javax.swing.*;

/**
 *
 * @author Nash
 */
public class Planning extends JFrame {
    
    
    public static int ligneCliquee;
    public static int colonneCliquee;
    public static int ligneModifiee;
    public static int colonneModifiee;
    public static String salleClique;
    public static String creneauClique;
    public static String dateP;    
    public static String nomFilmActuel;
    public static Film nouveauFilm = new Film();
    private int caseVoulu;
    
    public Planning() 
    {
        
        initComponents();
        affichageDuBonPlanning();
        remplirListeCategorieFilms();   

    }
    
    private void affichageDuBonPlanning()
    {
        String dateSelectionnee = datePlanning.getSelectedItem().toString();

        switch(dateSelectionnee)
        {
            case "12/05/2021" :
                planning1.setVisible(true);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                
                break;

            case "13/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(true);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
            
            case "14/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(true);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
            
            case "15/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(true);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
            
            case "16/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(true);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "17/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(true);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "18/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(true);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "19/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(true);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "20/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(true);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "21/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(true);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "22/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(true);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "23/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(true);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "24/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(true);
                planning14.setVisible(false);
                break;
                
            case "25/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(true);
                break;
        }
    }
    
        private void remplirListeCategorieFilms()
    {
        try
        {
            // Récupération de la liste des catégories de la base de données
            ArrayList<CategorieFilm> listeCategories = new ArrayList();
            CategorieRessource categorieManager = new CategorieRessource();
            listeCategories = categorieManager.recupererListeCategories();
            
            for (CategorieFilm uneCategorie : listeCategories) {
                String nomCat = uneCategorie.getNomCategorie();
                comboBoxFilm.addItem(nomCat);
            }
            
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e);
        }
            
    }
    
    private JTable ajoutFilmSurLeBonPlanning(String dateSelectionnee)
    {
        JTable planningARemplir = new JTable();
        
       switch(dateSelectionnee)
        {
            case "12/05/2021" :
                planningARemplir = planning1;
                break;

            case "13/05/2021" :
                planningARemplir = planning2;
                break;
            
            case "14/05/2021" :
                planningARemplir = planning3;
                break;
            
            case "15/05/2021" :
                planningARemplir = planning4;
                break;
            
            case "16/05/2021" :
                planningARemplir = planning5;
                break;
                
            case "17/05/2021" :
                planningARemplir = planning6;
                break;
                
            case "18/05/2021" :
                planningARemplir = planning7;
                break;
                
            case "19/05/2021" :
                planningARemplir = planning8;
                break;
                
            case "20/05/2021" :
                planningARemplir = planning9;
                break;
                
            case "21/05/2021" :
                planningARemplir = planning10;
                break;
                
            case "22/05/2021" :
                planningARemplir = planning11;
                break;
                
            case "23/05/2021" :
                planningARemplir = planning12;
                break;
                
            case "24/05/2021" :
                planningARemplir = planning13;
                break;
                
            case "25/05/2021" :
                planningARemplir =planning14;
                break;
                
            
        }
       
       return planningARemplir;
    }
    
    private void genererPlanning(String creneau, String date, String salle, String nomFilm)
    {
        // Ajout sur la bonne ligne 
        
        int ligneSelectionnee = 0;
        
        switch(creneau){
            case "Matin" :
                ligneSelectionnee = 1;
                break;

            case "Fin de matinée" :
                ligneSelectionnee = 2;
                break;

            case "Midi" :
                ligneSelectionnee = 3;
                break;

            case "Milieu d'après-midi" :
                ligneSelectionnee = 4;
                break;

            case "Fin d'après-midi":
                ligneSelectionnee = 5;
                break;

            case "Soirée" :
                ligneSelectionnee = 6;
                break;
        }
        // Ajout dans la bonne colonne
        int colonneSelectionnee = 0;

        switch(salle){
            case "Le Grand Theatre Lumière" :
                colonneSelectionnee = 1;
                break;

            case "La salle Debussy" :
                colonneSelectionnee = 2;
                break;

            case "La salle Bunuel" :
                colonneSelectionnee = 3;
                break;

            case "La salle du Soixantieme" :
                colonneSelectionnee = 4;
                break;

            case "La salle Bazin":
                colonneSelectionnee = 5;
                break;
        }

        // On sélectionne le bon planning à remplir en fonction de la date de la projection
        JTable planningARemplir = new JTable();
        planningARemplir = ajoutFilmSurLeBonPlanning(date);

        planningARemplir.setValueAt(nomFilm, ligneSelectionnee, colonneSelectionnee);
    }
            
    
    private void completerPlanning(JTable planning, java.awt.event.MouseEvent evt)
    {
        // Endroit et date où l'utilisateur a cliqué
        ligneCliquee = planning.rowAtPoint(evt.getPoint());
        colonneCliquee = planning.columnAtPoint(evt.getPoint());
        dateP = datePlanning.getSelectedItem().toString();
        
        
        if((ligneCliquee != 0) && (colonneCliquee != 0))
        {
            salleClique = planning.getValueAt(0, colonneCliquee).toString();
            //System.out.println("La salle correspond a : "+salleClique);
            
            creneauClique = planning.getValueAt(ligneCliquee, 0).toString();
            //System.out.println("Le créneau correspond à : "+creneauClique);
            
            // On ajoute un nouveau film
            if((planning.getValueAt(ligneCliquee,colonneCliquee) == null) || (planning.getValueAt(ligneCliquee,colonneCliquee) == ""))
            {
                AjoutFilms fenAjoutFilm = new AjoutFilms(this, true);   
                fenAjoutFilm.setVisible(true);
                
                String nomDuFilm = nouveauFilm.getNomFilm();
                
                if((nomDuFilm == null) ||(nomDuFilm.equals("")))
                {
                    //Rien à faire
                }
                else
                {
                    String nomFilm = nouveauFilm.getNomFilm();
                    
                    //On sélectionne la bonne ligne à remplir en fonction du créneau choisi dans la fenêtre AjoutFilms
                    String creneauChoisi = nouvelleProjection.getCrenauProjection();
                    int ligneSelectionnee = 0;
                    
                    switch(creneauChoisi){
                        case "Matin" :
                            ligneSelectionnee = 1;
                            break;
                        
                        case "Fin de matinée" :
                            ligneSelectionnee = 2;
                            break;
                            
                        case "Midi" :
                            ligneSelectionnee = 3;
                            break;
                            
                        case "Milieu d'après-midi" :
                            ligneSelectionnee = 4;
                            break;
                            
                        case "Fin d'après-midi":
                            ligneSelectionnee = 5;
                            break;
                            
                        case "Soirée" :
                            ligneSelectionnee = 6;
                            break;
                    }
                    
                    // On sélectionne la bonne colonne à remplir en fonction de la salle choisie dans la fenêtre AjoutFilms
                    String salleNom = nouvelleProjection.getSalleProjection().getNomSalle();
                    
                    int colonneSelectionnee = 0;
                    
                    switch(salleNom){
                        case "Le Grand Theatre Lumière" :
                            colonneSelectionnee = 1;
                            break;
                        
                        case "La salle Debussy" :
                            colonneSelectionnee = 2;
                            break;
                            
                        case "La salle Bunuel" :
                            colonneSelectionnee = 3;
                            break;
                            
                        case "La salle du Soixantieme" :
                            colonneSelectionnee = 4;
                            break;
                            
                        case "La salle Bazin":
                            colonneSelectionnee = 5;
                            break;
                    }
                    
                    // On sélectionne le bon planning à remplir en fonction de la date choisi sur la fenêtre AjoutFilms
                    JTable planningARemplir = new JTable();
                    planningARemplir = ajoutFilmSurLeBonPlanning(nouvelleProjection.getDateProjection());
                    
                    if(planningARemplir.getValueAt(ligneSelectionnee, colonneSelectionnee)==null || (planningARemplir.getValueAt(ligneSelectionnee,colonneSelectionnee) == ""))
                    {
                        planningARemplir.setValueAt(nomFilm, ligneSelectionnee, colonneSelectionnee);
                    }
                    else
                    {
                        int reponse = JOptionPane.showConfirmDialog(this, "Attention, il y a déjà un film à ce créneau. Voulez-vous le remplacer?", "Remplacement", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                        if(reponse == JOptionPane.YES_OPTION)
                        {
                            planningARemplir.setValueAt(nomFilm, ligneSelectionnee, colonneSelectionnee);
                        }
                        else
                        {
                            AjoutFilms fenAjoutFilm2 = new AjoutFilms(this, true);   
                            fenAjoutFilm2.setVisible(true);
                        }
                    }
                    
                    datePlanning.setSelectedItem(nouvelleProjection.getDateProjection());
                    
                    // On réinitialise l'objet Film 
                    nouveauFilm.setNomFilm(null);
                    nouveauFilm.setDuree(null);
                    nouveauFilm.setIdFilm(-1);
                    nouveauFilm.setListeCategories(null);
                    nouveauFilm.setListeProjections(null);
                    
                }                   
            } 
            else      // On modifie ou supprime un film
            {
     
                nomFilmActuel =  planning.getValueAt(ligneCliquee, colonneCliquee).toString();
                ModifierFilm fenModifFilm = new ModifierFilm(this,true);
                fenModifFilm.setVisible(true);
                
                if(suppression)
                {
                    planning.setValueAt(null, ligneCliquee, colonneCliquee);
                    suppression=false;
                }
                  
                else
                {
                    if(nouveauFilm2 != null)
                    {
                        String nomFilm = nouveauFilm2.getNomFilm(); 
                        String salleD = salleDest ;
                        String creneauD = creneauDest ;


                        JTable planningARemplir = new JTable();
                        System.out.println("Date ou remplir : "+nouvelleProjection.getDateProjection());
                        planningARemplir = ajoutFilmSurLeBonPlanning(nouvelleProjection.getDateProjection());

                        quelCase(planningARemplir,creneauD,salleD,nomFilm);
                        if(salleClique != salleD || creneauClique != creneauD)
                        {
                            planning.setValueAt(null, ligneCliquee, colonneCliquee);
                        }
                        
                        

                        datePlanning.setSelectedItem(nouvelleProjection.getDateProjection());


                        // On réinitialise l'objet Film 
                        nouveauFilm2.setNomFilm(null);
                        nouveauFilm2.setDuree(null);
                        nouveauFilm2.setIdFilm(-1);
                        nouveauFilm2.setListeCategories(null);
                        nouveauFilm2.setListeProjections(null);
                    }
  
                }
  
            }
        }
        
    }
    
    private void quelCase(JTable planning, String ligne, String colonne,String nomFilm)
    {
        
        
        if(ligne == "Matin")
        {
            if(colonne == "Le Grand Theatre Lumière")
            {
                planning.setValueAt(nomFilm,1 ,1 );
            }
            else if(colonne == "La salle Debussy")
            {
                planning.setValueAt(nomFilm,1 ,2 );
            }
            else if(colonne == "La salle Bunuel")
            {
                planning.setValueAt(nomFilm,1 ,3 );
            }
            else if(colonne == "La salle du Soixantieme")
            {
                planning.setValueAt(nomFilm,1 ,4 );
            }
            else if(colonne == "La salle Bazin")
            {
                planning.setValueAt(nomFilm,1 ,5 );
            }
        }
        else if(ligne == "Fin de matinée")
        {
            if(colonne == "Le Grand Theatre Lumière")
            {
                planning.setValueAt(nomFilm,2 ,1 );
            }
            else if(colonne == "La salle Debussy")
            {
                planning.setValueAt(nomFilm,2 ,2 );
            }
            else if(colonne == "La salle Bunuel")
            {
                planning.setValueAt(nomFilm,2 ,3 );
            }
            else if(colonne == "La salle du Soixantieme")
            {
                planning.setValueAt(nomFilm,2 ,4 );
            }
            else if(colonne == "La salle Bazin")
            {
                planning.setValueAt(nomFilm,2 ,5 );
            }
        }
        else if(ligne == "Midi")
        {
            if(colonne == "Le Grand Theatre Lumière")
            {
                planning.setValueAt(nomFilm,3 ,1 );
            }
            else if(colonne == "La salle Debussy")
            {
                planning.setValueAt(nomFilm,3 ,2 );
            }
            else if(colonne == "La salle Bunuel")
            {
                planning.setValueAt(nomFilm,3 ,3 );
            }
            else if(colonne == "La salle du Soixantieme")
            {
                planning.setValueAt(nomFilm,3 ,4 );
            }
            else if(colonne == "La salle Bazin")
            {
                planning.setValueAt(nomFilm,3 ,5 );
            }
        }
        else if(ligne == "Milieu d'après-midi")
        {
            if(colonne == "Le Grand Theatre Lumière")
            {
                planning.setValueAt(nomFilm,4 ,1 );
            }
            else if(colonne == "La salle Debussy")
            {
                planning.setValueAt(nomFilm,4,2 );
            }
            else if(colonne == "La salle Bunuel")
            {
                planning.setValueAt(nomFilm,4 ,3);
            }
            else if(colonne == "La salle du Soixantieme")
            {
                planning.setValueAt(nomFilm,4 ,4);
            }
            else if(colonne == "La salle Bazin")
            {
                planning.setValueAt(nomFilm,4 ,5);
            }
        }
        else if(ligne == "Fin d'après-midi")
        {
            if(colonne == "Le Grand Theatre Lumière")
            {
                planning.setValueAt(nomFilm,5 ,1 );
            }
            else if(colonne == "La salle Debussy")
            {
                planning.setValueAt(nomFilm,5 ,2 );
            }
            else if(colonne == "La salle Bunuel")
            {
                planning.setValueAt(nomFilm,5 ,3 );
            }
            else if(colonne == "La salle du Soixantieme")
            {
                planning.setValueAt(nomFilm,5 ,4 );
            }
            else if(colonne == "La salle Bazin")
            {
                planning.setValueAt(nomFilm,5 ,5 );
            }
        }
        else if(ligne == "Soirée")
        {
            if(colonne == "Le Grand Theatre Lumière")
            {
                planning.setValueAt(nomFilm,6 ,1 );
            }
            else if(colonne == "La salle Debussy")
            {
                planning.setValueAt(nomFilm,6 ,2);
            }
            else if(colonne == "La salle Bunuel")
            {
                planning.setValueAt(nomFilm,6 ,3 );
            }
            else if(colonne == "La salle du Soixantieme")
            {
                planning.setValueAt(nomFilm,6 ,4 );
            }
            else if(colonne == "La salle Bazin")
            {
                planning.setValueAt(nomFilm,6 ,5 );
            }
        }
        
    }
    
    


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        conteneurPlanning = new javax.swing.JPanel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        datePlanning = new javax.swing.JComboBox<>();
        conteneurTableau = new javax.swing.JPanel();
        planning1 = new javax.swing.JTable();
        planning2 = new javax.swing.JTable();
        planning2.setVisible(false);
        planning3 = new javax.swing.JTable();
        planning3.setVisible(false);
        planning4 = new javax.swing.JTable();
        planning4.setVisible(false);
        planning5 = new javax.swing.JTable();
        planning5.setVisible(false);
        planning6 = new javax.swing.JTable();
        planning6.setVisible(false);
        planning7 = new javax.swing.JTable();
        planning7.setVisible(false);
        planning8 = new javax.swing.JTable();
        planning8.setVisible(false);
        planning9 = new javax.swing.JTable();
        planning9.setVisible(false);
        planning10 = new javax.swing.JTable();
        planning10.setVisible(false);
        planning11 = new javax.swing.JTable();
        planning11.setVisible(false);
        planning12 = new javax.swing.JTable();
        planning12.setVisible(false);
        planning13 = new javax.swing.JTable();
        planning13.setVisible(false);
        planning14 = new javax.swing.JTable();
        planning14.setVisible(false);
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listeFilm = new javax.swing.JList<>();
        comboBoxFilm = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        param = new javax.swing.JButton();
        genererButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 273, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Planning des projection - Cannes");
        setLocation(new java.awt.Point(0, 0));
        setResizable(false);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Toutes les salles" }));

        jLabel1.setText("Pour ajouter un film, il suffit de double cliquer sur la case que l'on souhaite.");

        jLabel2.setText("Pour modifier un film, il suffit de double cliquer sur la case rempli que l'on souhaite.");

        jLabel3.setText("Lorsque il un petit rond vert, c'est qu'il y a des places disponibles.");

        jLabel4.setText("Lorsque il un petit rond reouge, c'est qu'il n'y a pas de places disponibles.");

        datePlanning.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "12/05/2021", "13/05/2021", "14/05/2021", "15/05/2021", "16/05/2021", "17/05/2021", "18/05/2021", "19/05/2021", "20/05/2021", "21/05/2021", "22/05/2021", "23/05/2021", "24/05/2021", "25/05/2021" }));
        datePlanning.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                datePlanningItemStateChanged(evt);
            }
        });
        datePlanning.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                datePlanningActionPerformed(evt);
            }
        });

        planning1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning1.setRowHeight(70);
        planning1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning1MouseClicked(evt);
            }
        });

        planning2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning2.setRowHeight(70);
        planning2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning2MouseClicked(evt);
            }
        });

        planning3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning3.setRowHeight(70);
        planning3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning3MouseClicked(evt);
            }
        });

        planning4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", "", null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning4.setRowHeight(70);
        planning4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning4MouseClicked(evt);
            }
        });

        planning5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning5.setRowHeight(70);
        planning5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning5MouseClicked(evt);
            }
        });

        planning6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning6.setRowHeight(70);
        planning6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning6MouseClicked(evt);
            }
        });

        planning7.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning7.setRowHeight(70);
        planning7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning7MouseClicked(evt);
            }
        });

        planning8.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning8.setRowHeight(70);
        planning8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning8MouseClicked(evt);
            }
        });

        planning9.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning9.setRowHeight(70);
        planning9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning9MouseClicked(evt);
            }
        });

        planning10.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning10.setRowHeight(70);
        planning10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning10MouseClicked(evt);
            }
        });

        planning11.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning11.setRowHeight(70);
        planning11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning11MouseClicked(evt);
            }
        });

        planning12.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning12.setRowHeight(70);
        planning12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning12MouseClicked(evt);
            }
        });

        planning13.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning13.setRowHeight(70);
        planning13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning13MouseClicked(evt);
            }
        });

        planning14.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"},
                {"Matin", "", null, null, null, null},
                {"Fin de matinée", null, null, null, null, null},
                {"Midi", null, null, null, "", null},
                {"Milieu d'après-midi", null, null, "", null, null},
                {"Fin d'après-midi", null, null, null, "", null},
                {"Soirée", null, null, null, null, null}
            },
            new String [] {
                "", "Le Grand Theatre Lumière", "La salle Debussy", "La salle Bunuel", "La salle du Soixantieme", "La salle Bazin"
            }
        ));
        planning14.setRowHeight(70);
        planning14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                planning14MouseClicked(evt);
            }
        });

        jScrollPane2.setViewportView(listeFilm);

        comboBoxFilm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxFilmActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboBoxFilm, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(69, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(comboBoxFilm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout conteneurTableauLayout = new javax.swing.GroupLayout(conteneurTableau);
        conteneurTableau.setLayout(conteneurTableauLayout);
        conteneurTableauLayout.setHorizontalGroup(
            conteneurTableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(conteneurTableauLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(planning1, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning3, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning4, javax.swing.GroupLayout.PREFERRED_SIZE, 945, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning5, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning6, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning8, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning9, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning10, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning11, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning12, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning13, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning14, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning7, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(planning2, javax.swing.GroupLayout.PREFERRED_SIZE, 934, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );
        conteneurTableauLayout.setVerticalGroup(
            conteneurTableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(conteneurTableauLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(conteneurTableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(conteneurTableauLayout.createSequentialGroup()
                        .addComponent(planning7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(planning8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(conteneurTableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(planning9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(planning10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(planning11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(planning12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(planning13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(planning14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(conteneurTableauLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(conteneurTableauLayout.createSequentialGroup()
                        .addComponent(planning2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, conteneurTableauLayout.createSequentialGroup()
                        .addGroup(conteneurTableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(planning6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(conteneurTableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(planning1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(planning3, javax.swing.GroupLayout.PREFERRED_SIZE, 515, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(planning4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(planning5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(10, 10, 10))))
        );

        jLabel5.setText("Sélectionner le jour du planning :");

        param.setText("Paramètres");
        param.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paramActionPerformed(evt);
            }
        });

        genererButton.setText("Générer un planning");
        genererButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                genererButtonMouseClicked(evt);
            }
        });
        genererButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genererButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout conteneurPlanningLayout = new javax.swing.GroupLayout(conteneurPlanning);
        conteneurPlanning.setLayout(conteneurPlanningLayout);
        conteneurPlanningLayout.setHorizontalGroup(
            conteneurPlanningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(conteneurPlanningLayout.createSequentialGroup()
                .addGroup(conteneurPlanningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(conteneurPlanningLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(conteneurPlanningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)))
                    .addGroup(conteneurPlanningLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(conteneurPlanningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(conteneurTableau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(conteneurPlanningLayout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(conteneurPlanningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(conteneurPlanningLayout.createSequentialGroup()
                                        .addComponent(datePlanning, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(554, 554, 554)
                                        .addComponent(param, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(genererButton)))))))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        conteneurPlanningLayout.setVerticalGroup(
            conteneurPlanningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(conteneurPlanningLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(conteneurPlanningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(datePlanning, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(param, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(genererButton, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(7, 7, 7)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(conteneurTableau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(conteneurPlanning, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(conteneurPlanning, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(489, 489, 489))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void planning14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning14MouseClicked
        completerPlanning(planning14, evt);
    }//GEN-LAST:event_planning14MouseClicked

    private void planning13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning13MouseClicked
        completerPlanning(planning13, evt);
    }//GEN-LAST:event_planning13MouseClicked

    private void planning12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning12MouseClicked
        completerPlanning(planning12, evt);
    }//GEN-LAST:event_planning12MouseClicked

    private void planning11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning11MouseClicked
        completerPlanning(planning11, evt);
    }//GEN-LAST:event_planning11MouseClicked

    private void planning10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning10MouseClicked
        completerPlanning(planning10, evt);
    }//GEN-LAST:event_planning10MouseClicked

    private void planning9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning9MouseClicked
        completerPlanning(planning9, evt);
    }//GEN-LAST:event_planning9MouseClicked

    private void planning8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning8MouseClicked
        completerPlanning(planning8, evt);
    }//GEN-LAST:event_planning8MouseClicked

    private void planning7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning7MouseClicked
        completerPlanning(planning7, evt);
    }//GEN-LAST:event_planning7MouseClicked

    private void planning6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning6MouseClicked
        completerPlanning(planning6, evt);
    }//GEN-LAST:event_planning6MouseClicked

    private void planning5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning5MouseClicked
        completerPlanning(planning5, evt);
    }//GEN-LAST:event_planning5MouseClicked

    private void planning4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning4MouseClicked
        completerPlanning(planning4, evt);
    }//GEN-LAST:event_planning4MouseClicked

    private void planning3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning3MouseClicked
        completerPlanning(planning3, evt);
    }//GEN-LAST:event_planning3MouseClicked

    private void planning2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning2MouseClicked
        completerPlanning(planning2, evt);
    }//GEN-LAST:event_planning2MouseClicked

    private void planning1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_planning1MouseClicked
        completerPlanning(planning1, evt);
    }//GEN-LAST:event_planning1MouseClicked

    private void datePlanningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_datePlanningActionPerformed

    }//GEN-LAST:event_datePlanningActionPerformed

    private void datePlanningItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_datePlanningItemStateChanged
        String dateSelectionnee = datePlanning.getSelectedItem().toString();

        switch(dateSelectionnee)
        {
            case "12/05/2021" :
                planning1.setVisible(true);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                
                break;

            case "13/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(true);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
            
            case "14/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(true);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
            
            case "15/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(true);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
            
            case "16/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(true);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "17/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(true);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "18/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(true);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "19/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(true);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "20/05/2021" :
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(true);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "21/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(true);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "22/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(true);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "23/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(true);
                planning13.setVisible(false);
                planning14.setVisible(false);
                break;
                
            case "24/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(true);
                planning14.setVisible(false);
                break;
                
            case "25/05/2021" :
                
                planning1.setVisible(false);
                planning2.setVisible(false);
                planning3.setVisible(false);
                planning4.setVisible(false);
                planning5.setVisible(false);
                planning6.setVisible(false);
                planning7.setVisible(false);
                planning8.setVisible(false);
                planning9.setVisible(false);
                planning10.setVisible(false);
                planning11.setVisible(false);
                planning12.setVisible(false);
                planning13.setVisible(false);
                planning14.setVisible(true);
                break;
        }
    }//GEN-LAST:event_datePlanningItemStateChanged

    private void comboBoxFilmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxFilmActionPerformed
        if(comboBoxFilm.getSelectedItem() == "Longs Métrages")
        {

        }
        else if(comboBoxFilm.getSelectedItem() == "Courts Métrages")
        {

        }
        else if(comboBoxFilm.getSelectedItem() == "Hors Compétition")
        {

        }
        else if(comboBoxFilm.getSelectedItem() == "Un Certain Regard")
        {

        }
    }//GEN-LAST:event_comboBoxFilmActionPerformed

    private void paramActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paramActionPerformed
        ParametreMenus fenParametre = new ParametreMenus();
        fenParametre.setVisible(true);
    }//GEN-LAST:event_paramActionPerformed

    private void genererButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genererButtonActionPerformed
      
    }//GEN-LAST:event_genererButtonActionPerformed

    private void genererButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_genererButtonMouseClicked
        FilmRessource filmManager = new FilmRessource();
        ArrayList<Film> listeFilms = new ArrayList();
        ArrayList<Projection> listeProjections = new ArrayList<Projection>();
        ProjectionRessource projectionManager = new ProjectionRessource();
        String dateProjection = null;
        String creneauProjection = null;
        String salleProjection = null;             

        // On récupère toute la liste des films dans la base de données
        listeFilms = filmManager.recupererListeFilms();
        
        // On récupère la liste de projection de chaque film
 
        for(Film unFilm : listeFilms)
        {
            listeProjections = projectionManager.recupererListeProjectionsPourUnFilm(unFilm.getIdFilm());
            
            // Pour chaque projection, on récupère la salle, la date et le créneau
            if(!listeProjections.isEmpty())
            {
                for(Projection uneProjection : listeProjections)
                {
                    dateProjection = uneProjection.getDateProjection();
                    creneauProjection = uneProjection.getCrenauProjection();

                    // Récupération de la salle de projection
                    salleProjection = projectionManager.recupererSalleProjection(uneProjection.getIdProjection());

                    // Affichage du film sur le planning
                    genererPlanning(creneauProjection, dateProjection, salleProjection, unFilm.getNomFilm());
  
                }
            }

        }
    }//GEN-LAST:event_genererButtonMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Planning.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Planning.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Planning.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Planning.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Planning fenetre = new Planning();
                fenetre.pack();
                fenetre.setLocationRelativeTo(null);
                fenetre.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> comboBoxFilm;
    private javax.swing.JPanel conteneurPlanning;
    private javax.swing.JPanel conteneurTableau;
    private javax.swing.JComboBox<String> datePlanning;
    private javax.swing.JButton genererButton;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JList<String> listeFilm;
    private javax.swing.JButton param;
    private javax.swing.JTable planning1;
    private javax.swing.JTable planning10;
    private javax.swing.JTable planning11;
    private javax.swing.JTable planning12;
    private javax.swing.JTable planning13;
    private javax.swing.JTable planning14;
    private javax.swing.JTable planning2;
    private javax.swing.JTable planning3;
    private javax.swing.JTable planning4;
    private javax.swing.JTable planning5;
    private javax.swing.JTable planning6;
    private javax.swing.JTable planning7;
    private javax.swing.JTable planning8;
    private javax.swing.JTable planning9;
    // End of variables declaration//GEN-END:variables

}
