/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fenetres;

import fenetres.CreationPlanning;

/**
 *
 * @author Anacoundaa1
 */
public class AppliCPOA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //je créé un objet de type Fenetre
        CreationPlanning premFen = new CreationPlanning();
        //J'adapte la taille de la fenetre à ses composants
        premFen.pack();
        //Je rends la fenetre visible
        premFen.setVisible(true);
        
    }
    
}
